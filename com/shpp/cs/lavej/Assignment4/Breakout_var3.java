package assignments.com.shpp.cs.lavej.Assignment4;

import acm.graphics.GLabel;
import acm.graphics.GObject;
import acm.graphics.GOval;
import acm.graphics.GRect;
import acm.util.RandomGenerator;
import com.shpp.cs.a.graphics.WindowProgram;
import java.awt.Color;
import java.awt.event.MouseEvent;

public class Breakout_var3 extends WindowProgram {

    /**
     * Width and height of application window in pixels
     */
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 500;

    /**
     * background color of app
     */
    public static final Color BG_COLOR = new Color(220, 220, 255);

    /**
     * Dimensions of game board (usually the same)
     */
    public static final int WIDTH = APPLICATION_WIDTH;
    public static final int HEIGHT = APPLICATION_HEIGHT;

    public static final double CENTER_OF_APP_WIDTH = WIDTH / 2;
    public static final double CENTER_OF_APP_HEIGHT = HEIGHT / 2;

    /**
     * Dimensions of the paddle
     */
    public static final int PADDLE_WIDTH = 60;
    public static final int PADDLE_HEIGHT = 10;

    /**
     * Offset of the paddle up from the bottom
     */
    public static final int PADDLE_Y_OFFSET = 30;

    /**
     * Number of bricks per row
     */
    public static final int NBRICKS_PER_ROW = 10;

    /**
     * Number of rows of bricks
     */
    public static final int NBRICK_ROWS = 10;

    /**
     * Separation between bricks
     */
    public static final int BRICK_SEP = 4;

    /**
     * Width of a brick
     */
    public static final int BRICK_WIDTH
            = (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / NBRICKS_PER_ROW;

    /**
     * Height of a brick
     */
    public static final int BRICK_HEIGHT = 8;

    /**
     * Radius of the ball in pixels
     */
    public static final int BALL_RADIUS = 10;

    /*constant of ball size*/
    public static final double BALL_DIAMETR = BALL_RADIUS * 2;

    /* ball collor, and changed collor (when ball punch the wall)*/
    public static final Color BALL_COLLOR_PUNCH = Color.BLUE;
    public static final Color BALL_COLLOR = Color.black;

    /**
     * Offset of the top brick row from the top
     */
    public static final int BRICK_Y_OFFSET = 70;

    /**
     * Number of turns
     */
    public static final int NTURNS = 3;

    /*Color of padle*/
    public static final Color PADDLE_COLOR = new Color(0, 0, 0);

    /*the top constant of paddle*/
    public static final double PADDLE_Y = (HEIGHT - PADDLE_HEIGHT) - PADDLE_Y_OFFSET;

    /*coordinates of paddle for draw in start of app*/
    public static final double PADDLE_X_CENTER = PADDLE_WIDTH / 2;
    /*coordinates of paddle for redraw if paddle reaches the application width*/
    public static final double PADDLE_X_MIN = 0;
    public static final double PADDLE_X_MAX = WIDTH - PADDLE_WIDTH;

    /*consist of number of bricks*/
    private int bricksCount = NBRICKS_PER_ROW * NBRICK_ROWS;

    /*Strings to print when game ends*/
    private static final String DRAW_WON = "You Win!!!";
    private static final String DRAW_LOOSE = "You Loose!!!";

    /*variables that will be parameters for move of the ball,
    to change location of object*/
    private static final double INIT_VALUE_FOR_VY = 5.0;
    private double vx;
    private double vy;

    /*method for drawing rectangle form objects(paddle,brick)
    input x,y,width,height,color;
    return rectangle;*/
    private GRect drawRectangleForm(double x, double y,
            double width, double height, Color color) {
        GRect rectangle = new GRect(x, y,
                width, height);
        rectangle.setFilled(true);
        rectangle.setFillColor(color);
        add(rectangle);
        return rectangle;
    }

    /*coloring the grid in differrent colors, 
    method take int of row
    return collor for this row*/
    private Color setGridColors(int j) {
        Color color = Color.red;
        if (j >= 2 && j < 4) {
            color = Color.orange;
        } else if (j >= 4 && j < 6) {
            color = Color.yellow;
        } else if (j >= 6 && j < 8) {
            color = Color.green;
        } else if (j >= 8 && j < 10) {
            color = Color.cyan;
        }
        return color;
    }

    /*method draw grid of bricks;*/
    private void drawGrid() {
        for (int j = 0; j < NBRICK_ROWS; j++) {
            for (int i = 0; i < NBRICKS_PER_ROW; i++) {
                GObject brickOfGrid = drawRectangleForm((BRICK_WIDTH * i) + (BRICK_SEP * i) + (BRICK_SEP / 2),
                        BRICK_Y_OFFSET + (BRICK_SEP * j) + (BRICK_HEIGHT * j),
                        BRICK_WIDTH, BRICK_HEIGHT, setGridColors(j));
            }
        }
    }

    /*Method for drawing Paddle
    return GRect;*/
    private GRect drawPaddle() {
        return drawRectangleForm(0, PADDLE_Y,
                PADDLE_WIDTH, PADDLE_HEIGHT, PADDLE_COLOR);
    }

    /*set the ball location to center of app*/
    private void setBallLocationToCenter(GOval ball) {
        ball.setLocation(CENTER_OF_APP_WIDTH - BALL_DIAMETR,
                CENTER_OF_APP_HEIGHT - BALL_DIAMETR);
    }

    /*method for draw ball
    return GOval;*/
    private GOval drawBall() {
        GOval ball = new GOval(CENTER_OF_APP_WIDTH - BALL_DIAMETR,
                CENTER_OF_APP_HEIGHT - BALL_DIAMETR,
                BALL_DIAMETR, BALL_DIAMETR);
        ball.setFilled(true);
        ball.setColor(BALL_COLLOR);
        add(ball);
        return ball;
    }

    /*method change color of the ball, and paused for some time,
    sticky walls may go from here
    input GObject*/
    private void changeColorOfObject(GObject obj, Color color) {
        obj.setColor(color);
        /*this pause give the chance to see ball colorchanging,
        but also ball little pause near the wall, 
        it can be filled with value more then 50 in speed of 10*/
        if (color == BALL_COLLOR_PUNCH) {
            pause(50);
        }
    }

    /*probability 50% of random value for rgen*/
    private boolean setProbability(RandomGenerator rgen) {
        return rgen.nextBoolean(0.5);
    }

    /*this method generates random value(from 1 to 3) for vx variable
    return double*/
    private double generateRandomValue() {
        RandomGenerator rgen = RandomGenerator.getInstance();
        double vx1 = 0.0;
        vx1 = rgen.nextDouble(1.0, 3.0);
        if (setProbability(rgen)) {
            vx1 = -vx1;
        }
        return vx1;
    }

    /*method checks is there wall on the left side of object,
    method takes GObject and returns boolean value*/
    private boolean isLeftIsBlocked(GOval ball) {
        boolean answer = false;
        if (ball.getX() <= 0) {
            answer = true;
            changeColorOfObject(ball, BALL_COLLOR_PUNCH);
        }
        return answer;
    }

    /*method checks is there wall on the top side of object,
    method takes GObject and returns boolean value*/
    private boolean isTopIsBlocked(GOval ball) {
        boolean answer = false;
        if (ball.getY() <= 0) {
            answer = true;
            changeColorOfObject(ball, BALL_COLLOR_PUNCH);
        }
        return answer;
    }

    /*method checks is there wall on the bottom side of object,
    method takes GObject and returns boolean value*/
    private boolean isBallLost(GOval ball) {
        boolean answer = false;
        if (ball.getY() + ball.getHeight() >= getHeight()) {
            answer = true;
            changeColorOfObject(ball, BALL_COLLOR_PUNCH);
        }
        return answer;
    }

    /*method checks is there wall on the right side of object,
    method takes GObject and returns boolean value*/
    private boolean isRightIsBlocked(GOval ball) {
        boolean answer = false;
        if (ball.getX() + ball.getWidth() >= getWidth()) {
            answer = true;
            changeColorOfObject(ball, BALL_COLLOR_PUNCH);
        }
        return answer;
    }

    /*method checks is there any object with same coordinates as ball has*/
    private GObject getObjectCollidingWithBall(GOval ball) {
        GObject obj = getElementAt(ball.getX() + ball.getWidth(),
                ball.getY() + ball.getHeight());
        if ((obj != null)) {
            return obj;
        }
        obj = getElementAt(ball.getX(),
                ball.getY() + ball.getHeight());
        if ((obj != null)) {
            return obj;
        }
        obj = getElementAt(ball.getX() + ball.getWidth(),
                ball.getY());
        if ((obj != null)) {
            return obj;
        }
        obj = getElementAt(ball.getX(),
                ball.getY());
        if ((obj != null)) {
            return obj;
        }
        return obj;
    }

    /*method checks is there any object with same coordinates as ball down 
    coordinates has*/
    private GObject getObjectCollidingWithBallAtDown(GOval ball) {
        GObject obj = getElementAt(ball.getX() + ball.getWidth(),
                ball.getY() + ball.getHeight());
        if ((obj != null)) {
            return obj;
        }
        obj = getElementAt(ball.getX(),
                ball.getY() + ball.getHeight());
        if ((obj != null)) {
            return obj;
        }

        return obj;
    }

    /*decrease the value of variable bricksCount*/
    private void decreaseBrickCounter() {
        bricksCount--;
    }

    /*inverse Value of vx*/
    private void inverseValueVX() {
        vx = -vx;
    }

    /*inverse Value of vy*/
    private void inverseValueVY() {
        vy = -vy;
    }

    /*set value of vy*/
    private void setStartValueOfVY() {
        vy = INIT_VALUE_FOR_VY;
    }

    /*method take the number of yor lifes,
    it also depends on the value of bricksCount,
    calls when the game ends*/
    private void countYourLifes(int yourLifes) {
        if (yourLifes == 0) {
            pause(100);
            removeAll();
            drawHeadline(DRAW_LOOSE);
        }
        if (bricksCount == 0) {
            drawHeadline(DRAW_WON);
        }
    }

    /*method of moving ball*/
    private void moveBall(GOval ball) {
        ball.move(vx, vy);
    }

    /*method of reflecting ball from walls(left,right)*/
    private void setBallBlockingByLeftRightWallBehavior(GOval ball) {
        if (isRightIsBlocked(ball)) {
            inverseValueVX();
        }
        if (isLeftIsBlocked(ball)) {
            inverseValueVX();
        }
    }

    /*method of reflecting ball from top wall*/
    private void setBallBlockingByTopWallBehavior(GOval ball) {
        if (isTopIsBlocked(ball)) {
            inverseValueVY();
        }
    }

    /*method of wall behavior when ball falls down to bottom,
    method take object ball and int number of lifes,
    and if it was called decrease value of int whic was inputed,
    and return it*/
    private int setBallIsLostBehavior(GOval ball, int yourLifes) {
        if (isBallLost(ball)) {
            yourLifes--;
            if (yourLifes > 0) {
                waitForClick();
                changeColorOfObject(ball, BALL_COLLOR);
                setBallLocationToCenter(ball);
                waitForClick();
                setStartValueOfVY();
            }
        }
        return yourLifes;
    }

    /*method of ball reflecting the paddle,takes ball, and takes ball collider from ball 
    down coordinates*/
    private void setBallCollidesWithPaddleBehavior(GOval ball, GObject objectCollidingWithBallAtDown) {
        boolean ballDownYIsEqualPaddleUpperY = ball.getY() + ball.getHeight() == PADDLE_Y;
        if (objectCollidingWithBallAtDown == paddle && ballDownYIsEqualPaddleUpperY) {
            changeColorOfObject(ball, BALL_COLLOR_PUNCH);
            inverseValueVY();
        }
    }

    /*method of ball behavior whith a brick*/
    private void setBallCollideWithBrickBehavior(GOval ball, GObject objectCollidingWithBall) {
        if (objectCollidingWithBall != paddle
                && objectCollidingWithBall != null) {
            changeColorOfObject(ball, BALL_COLLOR_PUNCH);
            remove(objectCollidingWithBall);
            decreaseBrickCounter();
            inverseValueVY();
        }
    }

    /*method of ball behavior when ball is free from colliding*/
    private void setBallFreeFromCollideBehavior(GOval ball) {
        changeColorOfObject(ball, BALL_COLLOR);
    }

    /*method which describes the behavior of the ball*/
    private void setBallBehavior(GOval ball) {
        while (true) {
            int yourLifes = NTURNS;
            vx = generateRandomValue();
            setStartValueOfVY();
            waitForClick();
            while (bricksCount != 0 && yourLifes > 0) {
                GObject objectCollidingWithBall = getObjectCollidingWithBall(ball);
                GObject objectCollidingWithBallAtDown = getObjectCollidingWithBallAtDown(ball);
                setBallBlockingByLeftRightWallBehavior(ball);
                setBallBlockingByTopWallBehavior(ball);
                yourLifes = setBallIsLostBehavior(ball, yourLifes);
                setBallCollidesWithPaddleBehavior(ball, objectCollidingWithBallAtDown);
                setBallCollideWithBrickBehavior(ball, objectCollidingWithBall);
                setBallFreeFromCollideBehavior(ball);
                moveBall(ball);
                pause(15);
            }
            countYourLifes(yourLifes);
            break;
        }
    }

    /*must calls when game ends, take string draw headline on the center 
    of app window*/
    private void drawHeadline(String s) {
        GLabel label = new GLabel(s);
        label.setFont("Verdana-30");
        label.setLocation(CENTER_OF_APP_WIDTH - label.getWidth() / 2,
                CENTER_OF_APP_HEIGHT - label.getHeight() / 2);
        label.setColor(Color.black);
        add(label);
    }

    GRect paddle;
    GOval Ball;

    public void run() {
        setBackground(BG_COLOR);
        paddle = drawPaddle();
        addMouseListeners();
        drawGrid();
        Ball = drawBall();
        setBallBehavior(Ball);
    }

    /*method calls when mouse is moving*/
    public void mouseMoved(MouseEvent e) {
        double startPaddlePositionX = e.getX();
        if (startPaddlePositionX >= PADDLE_X_MAX) {
            paddle.setLocation(PADDLE_X_MAX, PADDLE_Y);
        } else if (startPaddlePositionX <= PADDLE_X_CENTER) {
            paddle.setLocation(PADDLE_X_MIN, PADDLE_Y);
        } else {
            paddle.move(startPaddlePositionX - modifyedPaddlePositionX, 0);
        }
        modifyedPaddlePositionX = e.getX();
    }
    double modifyedPaddlePositionX;
}
