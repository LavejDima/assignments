package assignments.com.shpp.cs.lavej.Assignment3;

//import acm.graphics.GPoint;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;
import java.awt.Color;

/*Pyramid*/
public class Assignment3Part4 extends WindowProgram {

    /*aplication constants*/
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 300;
    /*bricks of pyramyd constants*/
    private final static int BRICK_HEIGHT = 15;
    private final static int BRICK_WIDTH = 26;
    private final static int BRICKS_IN_BASE = 10;

    /*Method makes brick, it takes points x,y(left upper corner of brick) to locate brick;
    color is set automaticly to orange
     */
    private void drawBrick(double x, double y) {
        GRect brick = new GRect(x, y, BRICK_WIDTH, BRICK_HEIGHT);
        brick.setFillColor(Color.orange);
        brick.setFilled(true);
        brick.setColor(Color.darkGray);
        //GPoint brickpoints = brick.getLocation();
        //System.out.println(brickpoints);
        add(brick);
    }

    /*Method draw line of bricks by calling drawBrick in loop,
    method takes as parameters aligningX number of ofset at x axis,
    alingningY - ofset at y axis,
    and decr - number of repeats*/
    private void drawLine(int aligningX, int alingningY, int decr) {

        for (int i = 0; i < decr; i++) {
            int w = (BRICK_WIDTH * i) + aligningX;
            int h = getHeight() - alingningY;
            drawBrick(w, h);

        }

    }

    /*Method returns the number of ofset by x axis the line of bricks that locates upper*/
    private int diagonalStepAlign() {
        int ofsetX = BRICK_WIDTH / 2;
        return ofsetX;
    }

    /*Method returns number of ofset by x axis, in line of bricks,
    if BRICKS_IN_BASE>HowMuchMayBeBricks it will be drawing to console:
    incorect aligning, decrease BRICK_WIDTH, or decrease BRICKS_IN_BASE 
                    BRICKS_IN_BASE>HowMuchMayBeBricks*/
    private int horizontalAlight() {
        int HowMuchMayBeBricks = getWidth() / BRICK_WIDTH;
        if (BRICKS_IN_BASE > HowMuchMayBeBricks) {
            System.out.println("incorect aligning, decrease BRICK_WIDTH, or decrease BRICKS_IN_BASE "
                    + "BRICKS_IN_BASE>HowMuchMayBeBricks");
        }
        int ofsetX = ((HowMuchMayBeBricks - BRICKS_IN_BASE) * BRICK_WIDTH) / 2;
        if (ofsetX < 0) {
            ofsetX = -ofsetX;
        }
        return ofsetX;

    }

    public void run() {
        /*write the aligning constatnts*/
        int verticalAlign = BRICK_HEIGHT;
        int horisontalAlign = horizontalAlight();
        /* the loop that calls drawLine method,
        and changing it parameters in each iteration*/
        for (int i = BRICKS_IN_BASE; i > 0; i--) {
            drawLine(horisontalAlign, verticalAlign, i);
            horisontalAlign += diagonalStepAlign();
            verticalAlign += BRICK_HEIGHT;
            //System.out.println("horisontalAlign is " + horisontalAlign);
            //System.out.println("verticalAlign is " + verticalAlign);

        }

    }

}
