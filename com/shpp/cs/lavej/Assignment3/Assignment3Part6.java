package assignments.com.shpp.cs.lavej.Assignment3;

import acm.graphics.GLabel;
import acm.graphics.GOval;
import acm.graphics.GPoint;
import acm.graphics.GPolygon;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;
import java.awt.Color;

/*Animation*/
public class Assignment3Part6 extends WindowProgram {

    public static final int APPLICATION_WIDTH = 650;
    public static final int APPLICATION_HEIGHT = 450;

    /**
     * The number of pixels to shift the label on each cycle
     */
    private static final double DELTA_X = 2.0;
    /**
     * The number of milliseconds to pause on each cycle
     */
    private static final int PAUSE_TIME = 10;
    /**
     * The string to use as the value of the label
     */
    private static final String HEADLINE
            = "Throught town "
            + "we walked at night...";

    /*method draw circles in loop,
    imitate stars,
    they are move less*/
    private void drawStars() {
        for (int i = 1; i < 30; i++) {
            for (int n = 1; n < 8; n++) {
                GOval star = new GOval(-50 + i * 55 + n * 5, -50 + n * 55, 5, 5);
                star.setFillColor(Color.blue);
                star.setFilled(true);
                add(star);

            }

        }
    }

    /*method draw circle, that must imitate falling star,
    method return GOval*/
    private GOval fallingStar() {
        GOval fallingStar = new GOval(-20, getHeight() / 4, 10, 10);
        fallingStar.setFillColor(Color.yellow);
        fallingStar.setFilled(true);
        add(fallingStar);
        return fallingStar;
    }

    /*Method draw polygon - triangle that immitates roof, it take GRect object,
    to get width of rectangle(house) to immitate roof,
    method returns triangle*/
    private GPolygon drawRoof(GRect rectangle) {
        GPoint point1 = new GPoint(rectangle.getX() - 5, rectangle.getY());
        GPoint point2 = new GPoint(rectangle.getWidth() + 5, rectangle.getY());
        GPoint point3 = new GPoint(rectangle.getWidth() / 2, rectangle.getY() - 50);
        GPoint[] gpoints = new GPoint[3];
        gpoints[0] = point1;
        gpoints[1] = point2;
        gpoints[2] = point3;
        GPolygon triangle = new GPolygon(gpoints);
        triangle.setFillColor(Color.black);
        triangle.setFilled(true);
        triangle.setColor(Color.gray);
        add(triangle);
        return triangle;
    }

    /*prepare the scene, make background black colored,
    draw the stars*/
    private void drawNight() {
        setBackground(Color.black);
        drawStars();
    }

    /*method draws GOval(Moon) return GOval*/
    private GOval drawMoon() {
        GOval moon = new GOval(0, getHeight(), getWidth() / 5, getWidth() / 5);
        moon.setFillColor(Color.white);
        moon.setFilled(true);
        add(moon);
        return moon;
    }

    /*method draws GRect(house),
    *takes x and y coordinate to locate houses
    *return GRect*/
    private GRect drawHouse(double x, double y) {
        GRect rectangle = new GRect(x, y, getWidth() / 8, getWidth() / 8);
        rectangle.setFillColor(Color.gray);
        rectangle.setFilled(true);
        rectangle.setColor(Color.white);
        add(rectangle);
        return rectangle;
    }

    /*method draws GRect(window),
    *takes GRect(house) to locate windows
    *return GRect*/
    private GRect drawWindow(GRect house) {
        GRect rectangle = new GRect(house.getX() + house.getWidth() / 3, house.getY() + house.getWidth() / 8, house.getWidth() / 4, house.getWidth() / 4);
        rectangle.setFillColor(Color.LIGHT_GRAY);
        rectangle.setFilled(true);
        rectangle.setColor(Color.white);
        add(rectangle);
        return rectangle;
    }

    /*method draws GLabel(label),
    *return GLabel*/
    private GLabel drawHeadLine() {
        GLabel label = new GLabel(HEADLINE);
        label.setFont("Times-30");
        label.setLocation(getWidth(), getHeight() / 2);
        label.setColor(Color.LIGHT_GRAY);
        add(label);
        return label;
    }

    public void run() {
        /*y coordinate to create house*/
        double gRect_Y = getHeight() - getWidth() / 10;

        /*prepare scen by drawNight()*/
        drawNight();
        /*creating objects*/
        GOval moon = drawMoon();
        GOval fallingStar = fallingStar();
        GRect house1 = drawHouse(0, gRect_Y);
        GRect house2 = drawHouse(house1.getWidth() + 15, gRect_Y);
        GRect window1 = drawWindow(house2);
        GRect house3 = drawHouse(-house1.getWidth() - 100, gRect_Y);
        GRect house4 = drawHouse(-house3.getWidth() - 500, gRect_Y);
        GRect window2 = drawWindow(house4);
        GRect house5 = drawHouse(-house3.getWidth() - 250, gRect_Y);
        GLabel label = drawHeadLine();
        GPolygon roof = this.drawRoof(house1);

        /*checking time*/
        long startTime = System.currentTimeMillis();
        long requestTime = System.currentTimeMillis() + 5000;
        /*moving objects in loop, only 5 seconds */
        //System.out.println("startTime = "+startTime);
        //System.out.println("requestTime = "+requestTime);
        while (startTime <= requestTime) {

            fallingStar.move(DELTA_X, 1);
            moon.move(DELTA_X - 1.5, -0.8);
            house1.move(DELTA_X, 0);
            roof.move(DELTA_X, 0);
            house2.move(DELTA_X, 0);
            house3.move(DELTA_X, 0);
            house4.move(DELTA_X, 0);
            window2.move(DELTA_X, 0);
            window1.move(DELTA_X, 0);
            house5.move(DELTA_X, 0);
            label.move(-DELTA_X + 0.7, 0);
            startTime = System.currentTimeMillis();
            pause(PAUSE_TIME);

        }
        //System.out.println("startTime after loop = " + startTime);
    }
}
