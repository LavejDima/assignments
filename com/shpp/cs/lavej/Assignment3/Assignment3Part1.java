package assignments.com.shpp.cs.lavej.Assignment3;

import acm.program.*;

/*Aerobics program*/
public class Assignment3Part1 extends Program {

    /*write and initialize variables*/
 /*array of input values of minutes*/
    private int[] minutesOfExercise = new int[7];

    private int numberOfDaysMore30 = 0;
    private int numberOfDaysMore40 = 0;
    private int more_30 = 0;
    private int more_40 = 0;

    /*this method take input value of minutes from user, 
    and put it to array minutesOfExercise[i]*/
    private void getNumberOfMinutes() {
        for (int i = 0; i < 7; i++) {
            int inc = i + 1;
            int dayi = readInt("How many minutes did you do on day " + inc + " ? ");
            minutesOfExercise[i] = dayi;
        }

    }

    /*method takes values from minutesOfExercise, 
    and check value of minutes for correctness, return true or false*/
    private boolean correctValue() {
        boolean value = false;
        int x = 0;
        for (int i = 0; i < minutesOfExercise.length; i++) {

            if ((minutesOfExercise[i]) <= 1440 && (minutesOfExercise[i]) >= 0) {
                x++;
            }
        }
        if (x == 7) {
            value = true;
        }
        return value;
    }

    /*method take value of minutes,
    and add one to value of numberOfDaysMore30
    if input minutes more then 30*/
    private int numberOfDaysMore30(int minutes) {

        if (minutes >= 30) {
            numberOfDaysMore30++;
        }

        return numberOfDaysMore30;
    }

    /*method take value of minutes,
    and add one to value of numberOfDaysMore40
    if input minutes more then 40*/
    private int numberOfDaysMore40(int minutes) {

        if (minutes >= 40) {
            numberOfDaysMore40++;
        }

        return numberOfDaysMore40;
    }

    /*method call method numberOfDaysMore30 in loop
    and give all values from array minutesOfExercise to him,
    method returns number of days vore than 30 min trainings*/
    private int retDaysMore_30Min() {

        for (int i = 0; i < 7; i++) {
            more_30 = numberOfDaysMore30(minutesOfExercise[i]);
        }
        return more_30;

    }

    /*method call method numberOfDaysMore40 in loop
    and give all values from array minutesOfExercise to him,
    method returns number of days vore than 40 min trainings*/
    private int retDaysMore_40Min() {
        for (int i = 0; i < 7; i++) {
            more_40 = numberOfDaysMore40(minutesOfExercise[i]);
        }
        return more_40;
    }

    /*main logic method that checks the prepared data and gives the answer
    what you should do*/
    private void verdict() {
        /*get number of days more then 30 and more then 40 min,
        and store it in variables*/
        int numberOfDaysThatMore30 = retDaysMore_30Min();
        int numberOfDaysThatMore40 = retDaysMore_40Min();

        /*check normal values, gives the answer*/
        println("Cardiovacular health:");
        if (numberOfDaysThatMore30 >= 5) {
            println("Great job! You've done enough exercise for cardiovacular health.");

        } else {
            int mustTrain = 5 - numberOfDaysThatMore30;
            println("You needed to train hard for at least "
                    + mustTrain + " more day(s) a week!");

        }

        println("Blood pressure: ");
        if (numberOfDaysThatMore40 >= 3) {
            println("Great job! You've done enough exercise for blood pressure.");

        } else {
            int mustTrain = 3 - numberOfDaysThatMore40;
            println("You needed to train hard for at least "
                    + mustTrain + " more day(s) a week!");

        }

    }

    public void run() {
        /*take values from user*/
        getNumberOfMinutes();
        /*check correctness*/
        if (correctValue()) {
            /*give the answer*/
            verdict();
        } else {
            println("Restart your programm and enter a correct value of minutes"
                    + " (more then 0, less then 1440)");
        }

    }

}
