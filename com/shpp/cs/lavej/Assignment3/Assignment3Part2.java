package assignments.com.shpp.cs.lavej.Assignment3;

import java.io.BufferedReader;
import java.io.InputStreamReader;


/*Grad numbers*/
public class Assignment3Part2 {

    private static int entNum = 0;

    /*method take the int frome console, store it in entNum variable*/
    private static void enterNumber() throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter a number: ");
        entNum = Integer.parseInt(reader.readLine());
    }

    /*main method which takes entNum after calling enterNumber() method,
    and go to 1 by solving*/
    private static void proceedNumber() {
        if (entNum % 2 == 0) {
            System.out.print(entNum + " is even so I take half: ");
            entNum = entNum / 2;
            System.out.println("get " + entNum + " ");
        } else if (entNum % 2 != 0) {
            System.out.print(entNum + " is odd so I make 3n + 1: ");
            entNum = entNum * 3 + 1;
            System.out.println("get " + entNum + " ");
        }

    }

    public static void main(String[] args) {
        try {
            /*get int frome console*/
            enterNumber();
            /*if int <=0, ask to put value >0*/
            if (entNum <= 0) {
                while (entNum <= 0) {
                    System.out.println("Enter a number > 0");
                    enterNumber();
                }
                /*until entNum value became 1 call proceedNumber()*/
                while (entNum != 1) {

                    proceedNumber();
                }
                System.out.println("The End.");
            } else /*else - if value is >0*/ {
                /*until entNum value became 1 call proceedNumber()*/
                while (entNum != 1) {

                    proceedNumber();
                }
                System.out.println("The End.");
            }

        } catch (Exception ex) {
            System.out.println("Please restart your programm and put another number of int value: > 0 but < " + Integer.MAX_VALUE);
        }

    }

}
