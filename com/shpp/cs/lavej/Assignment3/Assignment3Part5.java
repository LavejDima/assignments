package assignments.com.shpp.cs.lavej.Assignment3;

import acm.program.*;
import acm.util.RandomGenerator;

/*I put it all in 1 class, but i think, that it must be several classes Looser,Luckky etc.*/
 /*Casino-casino*/
public class Assignment3Part5 extends Program {

    /*number of dollars in table*/
    private static int dollars = 0;
    /*number of iterations*/
    private int numberOfIterations = 0;

    /*one of the gameplayers names looser*/
    class Looser {

        /*put the dollar to table*/
        private void putOneDollar() {
            Assignment3Part5.dollars += 1;
        }

        /*double the dollars on table*/
        private void addSum() {

            Assignment3Part5.dollars += Assignment3Part5.dollars;
        }

    }

    /*one of the gameplayers names lucky*/
    class Lucky {

        /*money of lucky*/
        private int luckyDollars = 0;

        /*lucky drops the monet*/
        private int dropMonet() {
            //int side = new Monet().inverseSide();
            int side = new Monet().sideOfMonet();
            return side;

        }
    }

    /*the class of monet and it behavior*/
    class Monet extends Program {

        /*method generate only 2 integers randomly 1 or 0*/
        private int sideOfMonet() {
            RandomGenerator rgen = new RandomGenerator();
            int sideOfMonet = rgen.nextInt(0, 1);
            return sideOfMonet;
        }

        /*inverse the value of sideOfMonet*/
        private int inverseSide() {
            int side = this.sideOfMonet();
            int sideOfMonet = 0;
            if (side == 1) {
                sideOfMonet = 0;
            } else if (side == 0) {
                sideOfMonet = 1;
            }
            return sideOfMonet;
        }

    }

    public void run() {
        /*create gameplayers*/
        Looser looser = new Looser();
        Lucky lucky = new Lucky();
        /*looser put one dolar at table*/
        looser.putOneDollar();

        /*game begins*/
 /*game will run if dollars that lucky have will be less then 20*/
        while (lucky.luckyDollars <= 20) {
            /*lucky drop the monet*/
            int side = lucky.dropMonet();
            if (side == 1) {
                /*double the dollars on table*/
                looser.addSum();
            } else {
                lucky.luckyDollars += dollars;
                int howMuchEarnedInGame = dollars;
                int total = lucky.luckyDollars;
                println("This game you earned: " + howMuchEarnedInGame);
                println("Your total is: " + total);
                /*lucky takes all money from table,
                reinitialize the dollars variable*/
                dollars = 0;
                /*looser put one dollar to table*/
                looser.putOneDollar();
                /*get the number of iterations*/
                numberOfIterations++;
            }

        }

        println("It took " + numberOfIterations + " games to earn $" + lucky.luckyDollars);

    }

}
