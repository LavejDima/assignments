package assignments.com.shpp.cs.lavej.a4_fractals;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class SierpinskiTriangle {

    //number of dots to create triangle
    public static final int DOTSCOUNT = 200000;
    private JFrame frame;
    private JPanel triangle;

    public static void main(String[] args) {
        new SierpinskiTriangle().run();
    }

    private void run() {
        //create new frame
        frame = new JFrame("Sierpinski triangle");
        triangle = new TriangleBuilder();
        //add triangle(jpanel) to frame
        frame.getContentPane().add(triangle, BorderLayout.CENTER);
        //to close correctly
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //size of frame
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

    private class TriangleBuilder extends JPanel {

        @Override
        public void paintComponent(Graphics g) {
            Point point = new Point(0, 0);
            for (int i = 0; i < DOTSCOUNT; i++) {
                getRandomDistBetweenPoints(point);
                putPixel(g, (int) point.getX(), (int) point.getY());
            }
        }

        /**
         * create black rectangle 1*1 in location of x,y
         *
         * @param g Graphics
         * @param x location
         * @param y location
         */
        private void putPixel(Graphics g, int x, int y) {
            g.setColor(Color.black);
            g.fillRect(x, y, 1, 1);
        }

        /**
         * return random corner(top-middle or bottom - right or bottom - left)
         *
         * @return Point of random corner
         */
        private Point chooseRandomCorner() {
            int randomInt = new Random().nextInt(3) + 1;
            if (randomInt == 1) {
                return new Point(triangle.getWidth() / 2, 0);
            } else if (randomInt == 2) {
                return new Point(0, triangle.getHeight());
            } else if (randomInt == 3) {
                return new Point(triangle.getWidth(), triangle.getHeight());
            }
            return null;
        }

        /**
         * set point into midle of current point and random located corner point
         *
         * @param point
         */
        private void getRandomDistBetweenPoints(Point point) {
            Point destination = chooseRandomCorner();
            double newX = (point.getX() + destination.getX()) / 2.0;
            double newY = (point.getY() + destination.getY()) / 2.0;
            point.setLocation(newX, newY);
        }
    }
}
