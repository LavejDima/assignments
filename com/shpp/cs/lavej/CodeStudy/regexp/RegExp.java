package assignments.com.shpp.cs.lavej.CodeStudy.regexp;

public class RegExp {

    String s = "sales1.xls\n"
            + "orders3.xls\n"
            + "sales2.xls\n"
            + "sales3.xls\n"
            + "Apa3c1.xls\n"
            + "eur\\ope2.xls\n"
            + "na1.xls\n"
            + "na2.xls\n"
            + "sa1.xls"
            + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcde\n"
            + "fghijklmnopqrstuvwxyz01234567890";

    String formula = "sin(90,5+Cos(145))/45-sqrt(sin(90)+144)^12/78*45";

    String date = "4/8/03\n"
            + "10-6-2004\n"
            + "2/2/2\n"
            + "01-01-01";

    public static void main(String[] args) {
        new RegExp().run();
    }

    private void run() {
        System.out.println(formula.replaceAll("[\\d-+*^()/]", "_").replaceAll("sin", "|"));
//System.out.println(date.replace(date.replaceAll("\\d{1,2}[-\\/]\\d{1,2}[-\\/]\\d{2,4}", ""),"|"));
    }

}
