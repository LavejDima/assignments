package assignments.com.shpp.cs.lavej.CodeStudy.GraphTesting;

import java.util.ArrayList;

public class Node {

    /**
     * name of city
     */
    private String name;
    /**
     * links of city
     */
    private ArrayList<String> links = new ArrayList<>();
    /**
     * flag of city
     */
    private boolean isWasHere = false;

    /**
     * Constructor of node class initialize String name
     *
     * @param name
     */
    public Node(String name) {
        setName(name);
    }

    /**
     * return name
     *
     * @return name of current city
     */
    public String getName() {
        return this.name;
    }

    /**
     * get all links of city
     *
     * @return arrayList with links of city
     */
    public ArrayList<String> getLinks() {
        return this.links;
    }

    /**
     * set name of city
     */
    private void setName(String name) {
        this.name = name;
    }

    /**
     * flag show us use we this class before or no
     *
     * @return must return true if we was here
     */
    public boolean isWasHere() {
        return this.isWasHere;
    }

    /**
     * set the value of variable wasIHere
     *
     * @param isWasHere
     */
    public void setIsWasHere(boolean isWasHere) {
        this.isWasHere = isWasHere;
    }

    /**
     * just easier to understand what happens
     *
     * @return ArrayList of links values
     */
    @Override
    public String toString() {
        return getLinks() + "";
    }

}
