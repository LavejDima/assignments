package assignments.com.shpp.cs.lavej.CodeStudy.GraphTesting;

public interface GraphWorkerConstants {

    /**
     * localization of file from which graph must be created
     */
    public static final String FILE_NAME = "CodeStudy/GraphTesting/assets/primitives.txt";
    //public static final String FILE_NAME = "CodeStudy/GraphTesting/assets/cities.txt";
}
