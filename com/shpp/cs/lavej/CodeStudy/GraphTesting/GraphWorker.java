package assignments.com.shpp.cs.lavej.CodeStudy.GraphTesting;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GraphWorker implements GraphWorkerConstants {

    HashMap<String, Node> graph = createGraphOfCitys(FILE_NAME);

    /**
     * create graph from cities in file
     *
     * @return graph
     */
    private HashMap<String, Node> createGraphOfCitys(String fileName) {
        HashMap<String, String> tempGraphOfCities = new HashMap<>();
        HashMap<String, Node> graphOfCities = new HashMap<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            while (true) {
                String s = reader.readLine();
                if (s == null) {
                    break;
                }
                String name = s.split(" ")[0];
                String link = s.split(" ")[1];
                if (!tempGraphOfCities.containsKey(name)) {
                    tempGraphOfCities.put(name, link);
                } else {
                    tempGraphOfCities.put(name, tempGraphOfCities.get(name) + "-" + link);
                }
                if (!tempGraphOfCities.containsKey(link)) {
                    tempGraphOfCities.put(link, name);
                } else {
                    tempGraphOfCities.put(link, tempGraphOfCities.get(link) + "-" + name);
                }
            }
            reader.close();
            for (String name : tempGraphOfCities.keySet()) {
                Node node = new Node(name);
                for (String link : tempGraphOfCities.get(name).split("-")) {
                    node.getLinks().add(link);
                }
                graphOfCities.put(name, node);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GraphWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GraphWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return graphOfCities;
    }

    /**
     * method of breadth first search in graph
     *
     * @param graph
     * @param beginFrom from where begin search
     */
    private void bfs(HashMap<String, Node> graph, String beginFrom) {
        try {
            LinkedBlockingDeque<String> deque = new LinkedBlockingDeque();
            deque.putLast(beginFrom);
            while (!deque.isEmpty()) {
                graph.get(deque.peekFirst()).setIsWasHere(true);
                System.out.println(deque.peek());
                for (String link : graph.get(deque.pollFirst()).getLinks()) {
                    if (!(deque.contains(link) || graph.get(link).isWasHere())) {
                        deque.putLast(link);
                    }
                }
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(GraphWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * method of deapth first search in graph
     *
     * @param graph
     * @param beginFrom from were begin search
     */
    private void dfs(String beginFrom) {
        try {
            LinkedBlockingDeque<String> deque = new LinkedBlockingDeque();
            deque.put(beginFrom);
            while (!deque.isEmpty()) {
                System.out.println(deque.peekLast());
                graph.get(deque.peekLast()).setIsWasHere(true);
                for (String link : graph.get(deque.pollLast()).getLinks()) {
                    if (!(deque.contains(link) || graph.get(link).isWasHere())) {
                        deque.putLast(link);
                    }
                }
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(GraphWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        new GraphWorker().run();
    }

    private void run() {
        System.out.println("        breadth first search");
        bfs(createGraphOfCitys(FILE_NAME), "a1");
        System.out.println();
        System.out.println("        depth first search");
        dfs("a1");
    }

}
