package assignments.com.shpp.cs.lavej.CodeStudy.StateMachineEmail;

import java.util.ArrayList;

public class EmailContainer {

    ArrayList<Enum> states = new ArrayList<>();
    ArrayList<Integer> locationOfCharsOfName = new ArrayList<>();
    ArrayList<Integer> locationOfCharsOfDomain = new ArrayList<>();
    ArrayList<Integer> locationOfCharsOfSite = new ArrayList<>();
    ArrayList<Integer> locationOfCharOfPoint = new ArrayList<>();
    ArrayList<Integer> locationOfCharOfAt = new ArrayList<>();
    String email = "";

    /**
     * init email in constructor
     *
     * @param email
     */
    public EmailContainer(String email) {
        this.email = email;
    }

    public String getName() {
        return getElement(locationOfCharsOfName, "Name");
    }

    public String getEmail() {
        return this.email;
    }

    public String getDomain() {
        return getElement(locationOfCharsOfDomain, "Domain");
    }

    public String getSite() {
        return getElement(locationOfCharsOfSite, "Site");
    }

    /**
     * returns the string whith specifide elements of email
     *
     * @param locationOfChars location of element
     * @param nameOfElement which we want to find
     * @return String element
     */
    private String getElement(ArrayList<Integer> locationOfChars, String nameOfElement) {
        String element = "";
        if (locationOfChars != null) {
            int elementSize = locationOfChars.size();
            while (elementSize != 0) {
                element
                        += email.charAt(locationOfChars.get(locationOfChars.size()
                                - elementSize));
                elementSize--;
            }
            return nameOfElement + ": " + element + " (number of symbols[" + locationOfChars.size() + "])";
        } else {
            return "Something wrong";
        }
    }

    /**
     * check for errors in ArrayList of states of email
     *
     * @return true if some errors exist
     */
    private boolean isSomeStateErrorsExist() {
        for (Enum e : states) {
            if (e == StatesOfEmail.ERROR) {
                return true;
            }
        }
        return false;
    }

    /**
     * check is there all of elements in email that must be
     *
     * @return true if all of elements of email exist
     */
    private boolean isAllElementExist() {
        if (states.contains(StatesOfEmail.NAME)
                && states.contains(StatesOfEmail.DOMAIN)
                && states.contains(StatesOfEmail.POINT)
                && states.contains(StatesOfEmail.AT)
                && states.contains(StatesOfEmail.SITE)) {
            return true;
        }
        return false;
    }

    /**
     * Override toString to teg more detailed information about EmailContainer
     * object
     *
     * @return String
     */
    @Override
    public String toString() {
        if (!isSomeStateErrorsExist() && isAllElementExist()) {
            return "\n" + getName() + "\n" + getSite() + "\n" + getDomain() + "\n";
        } else {
            return "\nSomething wrong\n";
        }
    }
}
