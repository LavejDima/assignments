package assignments.com.shpp.cs.lavej.CodeStudy.StateMachineEmail;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmailParser {

    /**
     * location of file with emails
     */
    public static final String FILE_NAME = "/home/lavej/NetBeansProjects/StateMachine/src/StateMachineEmail/emails.txt";

    /**
     * stub
     *
     * @param args
     */
    public static void main(String[] args) {
        new EmailParser().run();
    }

    /**
     * main method
     */
    private void run() {
        ArrayList<String> emailsList = getEmailsFromFile(FILE_NAME);
        ArrayList<EmailContainer> emailContainer = new ArrayList<>();
        for (String email : emailsList) {
            emailContainer.add(getContainerOfEmailInfo(email));
        }
        getEmailInfoByDomain("ru", emailContainer);
    }

    /**
     * method of finding email information by name of email
     *
     * @param name what name we find
     * @param emailContainer whe find name in this container
     */
    private void getEmailInfoByName(String name, ArrayList<EmailContainer> emailContainer) {
        for (EmailContainer container : emailContainer) {
            if (container.getName().contains(name)) {
                System.out.println("we search by "+name+":");
                System.out.println(container.getEmail() + container);
            }
        }
    }

    /**
     * find the email info in container
     *
     * @param domain the name of domain which we want find in base
     * @param emailContainer we find domain in this container
     */
    private void getEmailInfoByDomain(String domain, ArrayList<EmailContainer> emailContainer) {
        for (EmailContainer container : emailContainer) {
            if (container.getDomain().contains(domain)) {
                System.out.println("we search by "+domain+":");
                System.out.println(container.getEmail() + container);
            }
        }
    }

    /**
     *
     * @param site name of site that we want to find
     * @param emailContainer container in wich we must find
     */
    private void getEmailInfoBySite(String site, ArrayList<EmailContainer> emailContainer) {
        for (EmailContainer container : emailContainer) {
            if (container.getSite().contains(site)) {
                System.out.println("we search by "+site+":");
                System.out.println(container.getEmail() + container);
            }
        }
    }

    /**
     * main logic method of taking info from String with email and store it in
     * EmailContainer object
     *
     * @param email email
     * @return EmailContainer object
     */
    private EmailContainer getContainerOfEmailInfo(String email) {
        email = email.trim();
        EmailContainer container = new EmailContainer(email);
        StatesOfEmail emailState = StatesOfEmail.NAME;
        for (int i = 0; i < email.length(); i++) {
            char currentChar = email.charAt(i);
            if ((emailState == StatesOfEmail.NAME && currentChar == '.')
                    || (emailState == StatesOfEmail.NAME && Character.isLetterOrDigit(currentChar))) {
                container.locationOfCharsOfName.add(i);
                container.states.add(emailState);
            } else if (emailState == StatesOfEmail.NAME && currentChar == '@') {
                emailState = StatesOfEmail.AT;
                container.locationOfCharOfAt.add(i);
                container.states.add(emailState);
            } else if ((emailState == StatesOfEmail.AT || emailState == StatesOfEmail.SITE)
                    && Character.isLetterOrDigit(currentChar)) {
                emailState = StatesOfEmail.SITE;
                container.locationOfCharsOfSite.add(i);
                container.states.add(emailState);
            } else if (currentChar == '.' && emailState == StatesOfEmail.SITE) {
                emailState = StatesOfEmail.POINT;
                container.locationOfCharOfPoint.add(i);
                container.states.add(emailState);
            } else if ((emailState == StatesOfEmail.POINT || emailState == StatesOfEmail.DOMAIN)
                    && Character.isLetterOrDigit(currentChar)) {
                emailState = StatesOfEmail.DOMAIN;
                container.locationOfCharsOfDomain.add(i);
                container.states.add(emailState);
            } else {
                emailState = StatesOfEmail.ERROR;
                container.states.add(emailState);
            }
        }
        return container;
    }

    /**
     * get emails from file and put them into arrayList
     *
     * @param fileNameWithEmails String name of file were email list locates
     * @return ArrayList<String> of emails
     */
    private ArrayList<String> getEmailsFromFile(String fileNameWithEmails) {
        ArrayList<String> emailsList = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileNameWithEmails));
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                emailsList.add(line);
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmailParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmailParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return emailsList;
    }

}
