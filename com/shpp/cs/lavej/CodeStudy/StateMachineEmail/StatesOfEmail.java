package assignments.com.shpp.cs.lavej.CodeStudy.StateMachineEmail;

/**
 * The parts of email or error if there is some incorrectness
 */
public enum StatesOfEmail {
    NAME, AT, POINT, SITE, DOMAIN, ERROR;
}
