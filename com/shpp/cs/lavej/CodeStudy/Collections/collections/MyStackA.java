package assignments.com.shpp.cs.lavej.CodeStudy.Collections.collections;


public class MyStackA {

    private int maxSize;
    private char[] stackArray;
    private int top;

    public MyStackA(int maxSize) {
        this.maxSize = maxSize;
        stackArray = new char[maxSize];
        top = -1;
    }

    public void push(char element) {
        if (isFull()) {
            System.err.println("stack is full");
        } else {
            stackArray[++top] = element;
        }
    }

    public char pop() {
        if (isEmpty()) {
            System.err.println("stack is empty");
            return '\0';
        } else {
            return stackArray[top--];
        }
    }

    public char peek() {
        if (isEmpty()) {
            System.err.println("stack is empty");
            return '\0';
        } else {
            return stackArray[top];
        }
    }

    public boolean isFull() {
        return top == maxSize - 1;
    }

    public boolean isEmpty() {
        return top == -1;
    }
}
