package assignments.com.shpp.cs.lavej.CodeStudy.Collections.collections;

public class MyLinkedList {

    public class Link {

        private int key;
        public Link next;

        public Link(int key) {
            this.key = key;
        }

        public void displayLink() {
            System.out.println(key);
        }
    }

    private Link first;

    public MyLinkedList() {
        this.first = null;
    }

    public boolean isEmpty() {
        return this.first == null;
    }

    public void add(int element) {
        Link link = new Link(element);
        link.next = first;
        first = link;
    }

    public Link remove() {
        if (isEmpty()) {
            return null;
        } else {
            Link temp = first;
            first = first.next;
            return temp;
        }
    }

    public Link find(int key) {
        if (isEmpty()) {
            return null;
        } else {
            Link current = first;
            while (current.key != key) {
                if (current == null) {
                    return null;
                } else {
                    current = current.next;
                }
            }
            return current;
        }
    }

    public void displayList() {
        System.out.println("list ");
        Link current = first;
        while (current != null) {
            current.displayLink();
            current = current.next;
        }
        System.out.println();
    }
}
