package assignments.com.shpp.cs.lavej.CodeStudy.Collections.collections;


public class MyQueueA {

    private int maxSize;
    private int[] queueArray;
    private int front;
    private int back;
    private int numberOfItems;

    public MyQueueA(int maxSize) {
        this.maxSize = maxSize;
        queueArray = new int[this.maxSize];
        front = 0;
        back = -1;
        numberOfItems = 0;
    }

    public void add(int element) {
        if (isFull()) {
            System.err.println("queue is full");
        } else {
            queueArray[++back] = element;
            numberOfItems++;
        }
    }

    public int remove() {
        numberOfItems--;
        return queueArray[front++];
    }

    public int peek() {
        return queueArray[front];
    }

    public boolean isFull() {
        return numberOfItems == maxSize;
    }

    public boolean isEmpty() {
        return numberOfItems == 0;
    }

    public int size() {
        return numberOfItems;
    }
}
