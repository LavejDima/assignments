package assignments.com.shpp.cs.lavej.CodeStudy.Collections.collections;

public class MyPriorityQueueA {

    private int maxSize;
    private int[] priorityQueueArray;
    private int numberOfElements;

    public MyPriorityQueueA(int maxSize) {
        this.maxSize = maxSize;
        priorityQueueArray = new int[maxSize];
        numberOfElements = 0;
    }

    public boolean add(int element) {
        if (isFull()) {
            System.err.println("Priority queue is full. Element " + element + " is not added.");
            return false;
        } else {
            int i;
            if (isEmpty()) {
                priorityQueueArray[numberOfElements++] = element;
            } else {
                for (i = numberOfElements - 1; i >= 0; i--) {
                    if (element > priorityQueueArray[i]) {
                        priorityQueueArray[i + 1] = priorityQueueArray[i];
                    } else {
                        break;
                    }
                }
                priorityQueueArray[i + 1] = element;
                numberOfElements++;
            }
            return true;
        }
    }

    public int remove() {
        return priorityQueueArray[--numberOfElements];
    }

    public int peek() {
        return priorityQueueArray[numberOfElements - 1];
    }

    public boolean isEmpty() {
        return numberOfElements == 0;
    }

    public boolean isFull() {
        return numberOfElements == maxSize;
    }

    @Override
    public String toString() {
        String result = "";
        result += priorityQueueArray[0];
        for (int i = numberOfElements - 1; i >= 0; i--) {
            result += "," + priorityQueueArray[i];
        }
        return "[" + result + "]";
    }

    public int size() {
        return numberOfElements;
    }

}
