package assignments.com.shpp.cs.lavej.CodeStudy.Collections.collectionsTests;

import assignments.com.shpp.cs.lavej.CodeStudy.Collections.collections.MyPriorityQueueA;

public class MyPriorityQueueATest {

    private void run() {
        MyPriorityQueueA priorityQueue = new MyPriorityQueueA(6);
        priorityQueue.add(10);
        priorityQueue.add(46);
        priorityQueue.add(14);
        priorityQueue.add(15);
        priorityQueue.add(78);
        priorityQueue.add(3);
        priorityQueue.add(3);
        System.out.println("priorityQueue content " + priorityQueue);
        System.out.println("size before removing " + priorityQueue.size());
        System.out.println("remove number " + priorityQueue.remove());
        System.out.println("size after removing " + priorityQueue.size());
        System.out.println("size before removing " + priorityQueue.size());
        System.out.println("remove number " + priorityQueue.remove());
        System.out.println("size after removing " + priorityQueue.size());
        System.out.println("peek number " + priorityQueue.peek());
        System.out.println("size after peeking " + priorityQueue.size());
        System.out.println("priorityQueue content " + priorityQueue);

    }

    public static void main(String[] args) {
        new MyPriorityQueueATest().run();
    }
}
