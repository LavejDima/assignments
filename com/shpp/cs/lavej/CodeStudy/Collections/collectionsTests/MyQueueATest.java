package assignments.com.shpp.cs.lavej.CodeStudy.Collections.collectionsTests;

import assignments.com.shpp.cs.lavej.CodeStudy.Collections.collections.MyQueueA;

public class MyQueueATest {

    public static void main(String[] args) {
        new MyQueueATest().run();
    }

    private void run() {
        MyQueueA queue = new MyQueueA(6);
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);
        queue.add(6);
        System.out.println(queue.peek() + " size: " + queue.size());
        queue.remove();
        System.out.println(queue.peek() + " size: " + queue.size());
        queue.remove();
        System.out.println(queue.peek() + " size: " + queue.size());
        queue.remove();
        while (!queue.isEmpty()) {
            System.out.println(queue.remove() + " size: " + queue.size());
        }
    }
}
