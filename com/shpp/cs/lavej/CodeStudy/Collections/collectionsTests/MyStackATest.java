package assignments.com.shpp.cs.lavej.CodeStudy.Collections.collectionsTests;

import assignments.com.shpp.cs.lavej.CodeStudy.Collections.collections.MyStackA;

public class MyStackATest {

    public static final String SOME_STRING = "Hello World!";
    public static final String STRING_WITH_BRACKETS = "((())]";

    private void reverseCharsInWord(String word) {
        MyStackA stack = new MyStackA(word.length());
        for (int i = 0; i < word.length(); i++) {
            stack.push(word.charAt(i));
        }
        while (!stack.isEmpty()) {
            System.out.print(stack.pop() + " ");
        }
        System.out.println();
    }

    public void bracketCorrectnessCheck(String stringWithBrackets) {
        MyStackA stack = new MyStackA(stringWithBrackets.length());
        for (int i = 0; i < stringWithBrackets.length(); i++) {
            char currentChar = stringWithBrackets.charAt(i);
            if (currentChar == '(' || currentChar == '{' || currentChar == '[') {
                stack.push(currentChar);
            }
            if (currentChar == ')' || currentChar == '}' || currentChar == ']') {
                if (!stack.isEmpty()) {
                    char charFromStack = stack.pop();
                    if ((currentChar == ')' && charFromStack != '(')
                            || (currentChar == '}' && charFromStack != '{')
                            || (currentChar == ']' && charFromStack != '[')) {
                        System.err.println("incorrect folowing of brackets. Character " + currentChar + " at " + i + " position.");
                        break;
                    }
                } else {
                    System.err.println("incorrect ammount of brackets");
                    break;
                }
            }
        }
    }

    private void run() {
        reverseCharsInWord(SOME_STRING);
        bracketCorrectnessCheck(STRING_WITH_BRACKETS);
    }

    public static void main(String[] args) {
        new MyStackATest().run();
    }
}
