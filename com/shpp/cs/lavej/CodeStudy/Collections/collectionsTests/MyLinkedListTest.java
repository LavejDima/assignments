package assignments.com.shpp.cs.lavej.CodeStudy.Collections.collectionsTests;

import assignments.com.shpp.cs.lavej.CodeStudy.Collections.collections.MyLinkedList;

public class MyLinkedListTest {

    public void run() {
        MyLinkedList list = new MyLinkedList();
        list.add(10);
        list.add(45);
        list.add(60);
        list.add(34);
        list.add(4);
//        list.displayList();
        list.find(5).displayLink();

//        while (!list.isEmpty()) {
//            MyLinkedList.Link removedLink = list.remove();
//            System.out.print("removed ");
//            removedLink.displayLink();
//        }
//        list.displayList();
    }

    public static void main(String[] args) {
        new MyLinkedListTest().run();
    }

}
