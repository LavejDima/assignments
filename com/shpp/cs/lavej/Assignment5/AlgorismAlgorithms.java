package assignments.com.shpp.cs.lavej.Assignment5;

import com.shpp.cs.a.console.TextProgram;

public class AlgorismAlgorithms extends TextProgram {

    public void run() {
        while (true) {
            String number1 = getStringFromUser("enter first number:");
            String number2 = getStringFromUser("enter second number:");
            println(number1 + " + " + number2 + " = " + addNumericStrings(number1, number2));
            println();
        }
    }

    /**
     * method get input string from console and return it
     */
    private String getStringFromUser(String s) {
        return (readLine(s + " ").trim());
    }

    /**
     * turn string To ArrayOfChar
     *
     * @param s string to convert
     * @return chararray
     */
    private char[] convertStringToArrayOfChar(String s) {
        char[] ch = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            ch[i] = s.charAt(i);
        }
        return ch;
    }

    /**
     * method of adding two numbers
     *
     * @param n1
     * @param n2
     * @return n1+n2 result
     */
    private int addTwoNumbers(int n1, int n2) {
        return n1 + n2;
    }

    /**
     * set length of n1 and n2 arrays to equal value
     *
     * @param n1 array of int
     * @param n2 array of int
     * @return modified array of int
     */
    private int[] setToEqualArraysLength(int[] n1, int[] n2) {
        int[] modifiedArray = new int[n1.length > n2.length ? n1.length : n2.length];
        int n = 0;
        if (n1.length > n2.length) {
            modifiedArray = new int[n1.length];
            n = n2.length;
        }
        if (n1.length < n2.length) {
            modifiedArray = new int[n2.length];
            n = n1.length;
        }
        for (int i = modifiedArray.length - 1; i >= 0; i--) {
            if (n > 0) {
                modifiedArray[i] = n1.length > n2.length ? n2[n - 1] : n1[n - 1];
                n--;
            }
        }
        return modifiedArray;
    }

    /**
     * method add Values Of Int Arrays
     *
     * @param n1 array of int
     * @param n2 array of int
     * @return String result of adding values fro arraysof int
     */
    private String addValuesOfIntArrays(int[] n1, int[] n2) {
        String valueAfterAddingTwoIntegers = "";
        int[] addedIntegers = new int[n1.length];
        int remainder = 0;
        for (int i = addedIntegers.length - 1; i >= 0; i--) {
            addedIntegers[i] = addTwoNumbers(n1[i], n2[i]) + remainder;
            if (i < 1) {
                break;
            }
            if (addedIntegers[i] >= 10) {
                remainder = (addedIntegers[i] - addedIntegers[i] % 10) / 10;
                addedIntegers[i] = addedIntegers[i] % 10;
            } else {
                remainder = 0;
            }
        }
        for (int i = 0; i < addedIntegers.length; i++) {
            valueAfterAddingTwoIntegers += addedIntegers[i] + "";
        }
        return valueAfterAddingTwoIntegers;
    }

    /**
     * turn Values Of CharArray Into IntArray
     *
     * @param ch char array
     * @return int array
     */
    private int[] convertValuesOfCharArrayIntoIntArray(char[] ch) {
        int[] n = new int[ch.length];
        for (int i = 0; i < ch.length; i++) {
            n[i] = ch[i] - '0';
        }
        return n;
    }

    /**
     * Given two string representations of nonnegative integers, adds the
     * numbers represented by those strings and returns the result.
     *
     * @param n1 The first number.
     * @param n2 The second number.
     * @return A String representation of n1 + n2
     */
    private String addNumericStrings(String number1, String nunmber2) {
        int[] n1 = convertValuesOfCharArrayIntoIntArray(convertStringToArrayOfChar(number1));
        int[] n2 = convertValuesOfCharArrayIntoIntArray(convertStringToArrayOfChar(nunmber2));
        if (n1.length < n2.length) {
            n1 = setToEqualArraysLength(n1, n2);
        }
        if (n1.length > n2.length) {
            n2 = setToEqualArraysLength(n1, n2);
        }
        return addValuesOfIntArrays(n1, n2);
    }
}
