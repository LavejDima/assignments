package assignments.com.shpp.cs.lavej.Assignment5;

import com.shpp.cs.a.console.TextProgram;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CsvParsing extends TextProgram {

    /**
     * name of file to load (you must input name of your file here), and number
     * of line that we must to read from file; NUMBER_OF_COLUMN begins from 1;
     */
    public static final String FILE_NAME = "/home/lavej/Загрузки/test.csv";
    public static final int NUMBER_OF_COLUMN = 3;

    public void run() {
        try {
            printValuesOfArrayList(getColumnFromFile(FILE_NAME, NUMBER_OF_COLUMN));
        } catch (IndexOutOfBoundsException | NullPointerException ex) {
            println("Something went wrong");
            println("check NUMBER_OF_COLUMN");
            println("there is no such column");
        } catch (Exception ex) {
            println("Something went wrong");
            Logger.getLogger(CsvParsing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method takes fileName of file to open, and line wich to read, returns
     * String which consist of line that was readed;
     *
     * @param fileName name of file to open
     * @param numberrOfLine number of line in file which to read
     * @return String which is line from file that we read
     */
    private ArrayList<String> readFile(String fileName) {
        ArrayList<String> sArray = new ArrayList<>();
        try {
            BufferedReader reader
                    = new BufferedReader(new FileReader(fileName));
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                sArray.add(line);
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            println("Something went wrong");
            println("check the values of FILE_NAME for correctness.");
            println("File Not Found");
        } catch (IOException ex) {

            println("Something went wrong");
            println("with input or output of file");
            Logger.getLogger(CsvParsing.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sArray;
    }

    /**
     * Method takes String and make array of string from it by splitting parts
     * of String, divided by commas, then put the values from array of String to
     * ArrayList;
     *
     * @param inputString String to convert to ArrayList, splitting String by","
     * @return ArrayList of Strings
     */
    private ArrayList<String> convertStringToArrayList(String inputString) {
        ArrayList<String> lineOfFile = new ArrayList<>();
        char[] myCh = setVLineIfThereWasCommaBetweenQuets(convertStringToCharArray(inputString));
        inputString = convertCharArrayToString(myCh);
        String[] commasToken = inputString.split(",");
        for (int i = 0; i < commasToken.length; i++) {
            myCh = convertStringToCharArray(commasToken[i]);
            myCh = setComaInsteadOfVLine(myCh);
            commasToken[i] = deleteQoutes(convertCharArrayToString(myCh));
            lineOfFile.add(commasToken[i]);
        }
        return lineOfFile;
    }

    /**
     * Method takes String and convert it to char array;
     *
     * @param string string to convert
     * @return char array
     */
    private char[] convertStringToCharArray(String string) {
        char[] charArray = new char[string.length()];
        for (int i = 0; i < charArray.length; i++) {
            charArray[i] = string.charAt(i);
        }
        return charArray;
    }

    /**
     * Method takes char array and put "|" if there was "," between \"\";
     *
     * @param charArray char array to modify
     * @return modificated char array with "." instead "," in between \"\"
     */
    private char[] setVLineIfThereWasCommaBetweenQuets(char[] charArray) {
        char[] modifiedCharArray = new char[charArray.length];
        int n = 0;
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == '"') {
                charArray[i] = '"';
                n++;
            }
            if (charArray[i] == ',' && n % 2 == 1) {
                charArray[i] = '|';
            }
            modifiedCharArray[i] = charArray[i];
        }
        return modifiedCharArray;
    }

    /**
     * Method takes char array an convert it to string;
     *
     * @param charArray char array to convert to string
     * @return string which consist of chars from char array
     */
    private String convertCharArrayToString(char[] charArray) {
        String string = "";
        for (int i = 0; i < charArray.length; i++) {
            string += string.valueOf(charArray[i]);
        }
        return string;
    }

    /**
     * Method takes ArrayList<String> and print it values to console;
     *
     * @param columnInFile ArrayList of Strings to print to console
     */
    private void printValuesOfArrayList(ArrayList<String> columnInFile) {
        for (int i = 0; i < columnInFile.size(); i++) {
            println(columnInFile.get(i));
        }
    }

    /**
     * method returns ArrayList(columns) of ArrayList(lines) of Strings
     *
     * @param fileName name of file to parse
     * @return ArrayList(columns) of ArrayList(lines) of Strings
     */
    private ArrayList<ArrayList<String>> getFieldsFromFile(String fileName) {
        ArrayList<ArrayList<String>> lineOfFileInArrayList = new ArrayList<>();
        int lengthOfFileValues = readFile(fileName).size();
        for (int i = 0; i < lengthOfFileValues; i++) {
            String column = readFile(fileName).get(i);
            lineOfFileInArrayList.add(convertStringToArrayList(column));
        }
        return lineOfFileInArrayList;
    }

    /**
     * Tokenizes the input CSV file line and returns all the fields in that
     * line.
     *
     * @param numberOfColumn A line from a CSV file.
     * @return A list of all the tokens in that line, with any external
     * quotation marks removed.
     */
    private ArrayList<String> getColumnFromFile(String fileName, int numberOfColumn) {
        ArrayList<String> columnInArrayList = new ArrayList<>();
        for (int i = 0; i < getFieldsFromFile(fileName).size(); i++) {
            columnInArrayList.add(getFieldsFromFile(fileName).get(i).get(numberOfColumn - 1));
        }
        return columnInArrayList;
    }

    /**
     * set Coma InsteadOf vertikal line
     *
     * @param charArray take array of char
     * @return modified char array
     */
    private char[] setComaInsteadOfVLine(char[] charArray) {
        char[] modifiedCharArray = new char[charArray.length];
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == '|') {
                charArray[i] = ',';
            }
            modifiedCharArray[i] = charArray[i];
        }
        return modifiedCharArray;
    }

    /**
     * deleteQoutes
     *
     * @param string take string
     * @return string whithout qoutes
     */
    private String deleteQoutes(String string) {
        char[] modifiedCharArray = convertStringToCharArray(string);
        for (int i = 0; i < modifiedCharArray.length - 1; i++) {
            if (modifiedCharArray[i] == '"'
                    && modifiedCharArray[i + 1] == '"') {
                modifiedCharArray[i] = '_';
            } else if ((Character.isLetterOrDigit(modifiedCharArray[i])
                    || modifiedCharArray[i] == ' ')
                    && modifiedCharArray[i + 1] == '"') {
                modifiedCharArray[i + 1] = '_';
            } else if (i == 0 && modifiedCharArray[i] == '"') {
                modifiedCharArray[i] = '_';
            }
        }
        string = convertCharArrayToString(modifiedCharArray);
        return string.replaceAll("_", "");
    }
}
