package assignments.com.shpp.cs.lavej.Assignment5;

import com.shpp.cs.a.console.TextProgram;

public class SyllableCounting extends TextProgram {

    private static final String VOWELS_STRING = "aeyuio";

    public void run() {
        /* Repeatedly prompt the user for a word and print out the estimated
         * number of syllables in that word.  
         */
        while (true) {
            String word = readLine("Enter a single word: ");
            println("Syllable count: " + syllablesIn(word));
        }
    }

    /**
     * check is there vowels in char array
     *
     * @param ch array of chars
     * @return true if there is vowels in char array
     */
    private boolean checkIsThereVowels(char ch) {
        boolean answer = false;
        ch = Character.toLowerCase(ch);
        if (VOWELS_STRING.indexOf(ch) != -1) {
            answer = true;
        }
        return answer;
    }

    /**
     * how many vowels
     *
     * @param word String with single word
     * @return int value of vowels
     */
    private int checkHowManyVowelsInWord(String word) {
        int numberOfVowels = 0;
        for (int i = 0; i < word.length(); i++) {
            char ch = Character.toLowerCase(word.charAt(i));
            if (VOWELS_STRING.indexOf(ch) != -1) {
                numberOfVowels++;
            }
        }
        return numberOfVowels;
    }

    /**
     * is there vowel Before Vowel in string
     *
     * @param word string with word
     * @return true if there any vowelBeforeVowel
     */
    private boolean checkIsThereVowelBeforeVowel(String word) {
        boolean answer = false;
        for (int i = 0; i < word.length() - 1; i++) {
            char ch = Character.toLowerCase(word.charAt(i));
            if (VOWELS_STRING.indexOf(ch) != -1) {
                if (checkIsThereVowels(word.charAt(i))
                        == checkIsThereVowels(word.charAt(i + 1))) {
                    answer = true;
                }
            }
        }
        return answer;
    }

    /**
     * how Much Vowel Before Vowel
     *
     * @param word string of word
     * @return int howMuchVowelBeforVowel
     */
    private int checkHowMuchVowelBeforeVowel(String word) {
        int num = 0;
        for (int i = 0; i < word.length() - 1; i++) {
            char ch = Character.toLowerCase(word.charAt(i));
            if (VOWELS_STRING.indexOf(ch) != -1) {
                if (checkIsThereVowels(word.charAt(i))
                        == checkIsThereVowels(word.charAt(i + 1))) {
                    num++;
                }
            }
        }
        return num;
    }

    /**
     * Method cheks if there any 'e' at the and of a word
     *
     * @param word string word
     * @return true if there is e at the end, and false if there isnt
     */
    private boolean checkEAtTheEndOfWord(String word) {
        boolean answer = false;
        char ch = word.charAt(word.length() - 1);
        if (ch == 'e') {
            answer = true;
        }
        return answer;
    }

    /**
     * Given a word, estimates the number of syllables in that word according to
     * the heuristic specified in the handout.
     *
     * @param word A string containing a single word.
     * @return An estimate of the number of syllables in that word.
     */
    private int syllablesIn(String word) {
        int numberOfSyllabys = 1;
        numberOfSyllabys = checkHowManyVowelsInWord(word);
        if (checkEAtTheEndOfWord(word)) {
            numberOfSyllabys = numberOfSyllabys - 1;
        }
        if (checkIsThereVowelBeforeVowel(word)) {
            numberOfSyllabys = numberOfSyllabys - checkHowMuchVowelBeforeVowel(word);
        }
        if (numberOfSyllabys < 1) {
            numberOfSyllabys = 1;
        }
        return numberOfSyllabys;
    }
}
