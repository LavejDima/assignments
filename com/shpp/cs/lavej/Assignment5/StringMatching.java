package assignments.com.shpp.cs.lavej.Assignment5;

import com.shpp.cs.a.console.TextProgram;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class StringMatching extends TextProgram {

    /**
     * name of file to load (you must input name of your file here)
     */
    private static final String FILE_NAME
            = "/home/lavej/assignments/com/shpp/cs/lavej/Assignment5/en-dictionary.txt";

    public void run() {
        ArrayList<String> wordsFromFile = readFile(FILE_NAME);
        while (true) {
            allMatchesFor(getInputStringFromUser(), wordsFromFile);
        }
    }

    /**
     * method get input string from console and return it
     */
    private String getInputStringFromUser() {
        return readLine("print some letters: ");
    }

    /**
     * method reads file from fileName path
     *
     * @param fileName String - location and name of the file
     * @return ArrayList<String> all lines from file which was specified by name
     * of file
     */
    private ArrayList<String> readFile(String fileName) {
        ArrayList<String> namesfromDictionary = new ArrayList();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                namesfromDictionary.add(line);
            }
            reader.close();
        } catch (IOException e) {
            println("something wrong IOException in method readFile:"
                    + "namesfromDictionary init not correctly");
        }
        return namesfromDictionary;
    }

    /**
     * method checks matches of 2 strings
     *
     * @param string1 the string from user
     * @param string2 the string from file
     * @return true if there is matching
     */
    private boolean checkStringMatch(String string1, String string2) {
        boolean answer = true;
        string1 = string1.toLowerCase();
        string2 = string2.toLowerCase();
        char[] charsArrayFromString1 = new char[string1.length()];
        int n = 0;
        int lengthOfString2 = string2.length();
        int lengthOfString1 = string1.length();
        for (int i = 0; i < lengthOfString1; i++) {
            charsArrayFromString1[i] = string1.charAt(i);
            n = string2.indexOf(charsArrayFromString1[i]);
            if (n != -1 && lengthOfString2 > 0) {
                string2 = (string2.substring(n + 1));
                lengthOfString2--;
            } else {
                answer = false;
            }
        }
        return answer;
    }

    /**
     * method cheks matches of input from user string and lines from file and
     * the put matches to ArrayList<String> which returns
     *
     * @param letters string from user
     * @param allWords ArrayList<String> that consists of all lines from file;
     * @return ArrayList<String> which consist of lines from file that matched
     * with letters
     */
    private ArrayList<String> allMatchesFor(String letters, ArrayList<String> allWords) {
        ArrayList<String> matchedWords = new ArrayList<String>();
        for (int i = 0; i < allWords.size() - 1; i++) {
            if (checkStringMatch(letters, allWords.get(i))) {
                println(allWords.get(i));
                matchedWords.add(allWords.get(i));
            }
        }
        return matchedWords;
    }

}
