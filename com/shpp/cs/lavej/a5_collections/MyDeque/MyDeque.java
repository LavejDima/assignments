package assignments.com.shpp.cs.lavej.a5_collections.MyDeque;

import assignments.com.shpp.cs.lavej.a5_collections.MyLinkedList.MyLinkedList;

public class MyDeque<E> {

    //linked list class field
    private MyLinkedList<E> linkedList;

    /**
     * initialize Deque by creating new linked list
     */
    public MyDeque() {
        linkedList = new MyLinkedList<>();
    }

    /**
     * put element to last position
     *
     * @param element to put
     */
    public void putLast(E element) {
        linkedList.putLast(element);
    }

    /**
     * put element to first position
     *
     * @param element to put
     */
    public void putFirst(E element) {
        linkedList.putFirst(element);
    }

    /**
     * get first element without removing it
     *
     * @return first element in deque
     */
    public E peekFirst() {
        return linkedList.peekFirst();
    }

    /**
     * get last element without removing it
     *
     * @return last element from deque
     */
    public E peekLast() {
        return linkedList.peekLast();
    }

    /**
     * get first element and remove it from deque
     *
     * @return first element
     */
    public E pollFirst() {
        return linkedList.removeFirst();
    }

    /**
     * get last element and remove it from deque
     *
     * @return last element
     */
    public E pollLast() {
        return linkedList.removeLast();
    }

    /**
     * return size
     *
     * @return size of deque
     */
    public int size() {
        return linkedList.size();
    }

    /**
     * is deque empty
     *
     * @return true if deque is empty
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * check if deque contains such element
     *
     * @param element to find
     * @return true if deque contains such element
     */
    public boolean contains(E element) {
        return linkedList.find(element) != null;
    }
}
