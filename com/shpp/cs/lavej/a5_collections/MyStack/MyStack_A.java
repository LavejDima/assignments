package assignments.com.shpp.cs.lavej.a5_collections.MyStack;

import assignments.com.shpp.cs.lavej.a5_collections.MyArrayList.MyArrayList;

public class MyStack_A<E> {

    private MyArrayList<E> arrayList;
    //size of stack
    private int size;

    public MyStack_A() {
        this.arrayList = new MyArrayList<>();
    }

    /**
     * push element to stack
     *
     * @param element
     */
    public void push(E element) {
        arrayList.add(element);
        size++;
    }

    /**
     * get element from stack and remove it
     *
     * @return last added element
     */
    public E pop() {
        return isEmpty() ? null : arrayList.remove(--size);
    }

    /**
     * get element from stack without removing it
     *
     * @return last added element
     */
    public E peek() {
        return isEmpty() ? null : arrayList.get(size() - 1);
    }

    /**
     * size of stack
     *
     * @return number of elements in stack
     */
    public int size() {
        return size;
    }

    /**
     * is stack empty
     *
     * @return true if stack is empty
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public String toString() {
        return String.valueOf(arrayList);
    }

}
