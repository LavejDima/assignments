package assignments.com.shpp.cs.lavej.a5_collections.MyStack;

import assignments.com.shpp.cs.lavej.a5_collections.MyLinkedList.MyLinkedList;

public class MyStack_LL<E> {

    private MyLinkedList<E> linkedList;

    public MyStack_LL() {
        linkedList = new MyLinkedList<>();
    }

    /**
     * push element to stack. put it in beginning
     *
     * @param element
     */
    public void push(E element) {
        linkedList.putFirst(element);
    }

    /**
     * get element from stack without removing it
     *
     * @return last added element
     */
    public E peek() {
        return linkedList.peekFirst();
    }

    /**
     * get element from stack and remove it
     *
     * @return last added element
     */
    public E pop() {
        return linkedList.removeFirst();
    }

    /**
     * is stack empty
     *
     * @return true if stack is empty
     */
    public boolean isEmpty() {
        return linkedList.isEmpty();
    }

    /**
     * size of stack
     *
     * @return number of elements in stack
     */
    public int size() {
        return linkedList.size();
    }

    @Override
    public String toString() {
        return linkedList + "";
    }

}
