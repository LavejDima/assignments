package assignments.com.shpp.cs.lavej.a5_collections.MyLinkedList;

public class MyLinkedList<E> {

    //first element of linked list
    private Link<E> first;
    //last element of linked list
    private Link<E> last;
    //number of elements in linked list
    private int size;

    /**
     * check if there is any element in list
     *
     * @return true if list is empty
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * put first element
     *
     * @param element
     */
    public void putFirst(E element) {
        Link<E> newLink = new Link<>(element);
        if (isEmpty()) {
            last = newLink;
        } else {
            first.previous = newLink;
            newLink.next = first;
        }
        first = newLink;
        size++;
    }

    /**
     * put last element
     *
     * @param element
     */
    public void putLast(E element) {
        Link<E> newLink = new Link<>(element);
        if (isEmpty()) {
            first = newLink;
        } else {
            last.next = newLink;
            newLink.previous = last;
        }
        last = newLink;
        size++;
    }

    /**
     * get first element from list
     *
     * @return first element if there is no elements return null
     */
    public E peekFirst() {
        return isEmpty() ? null : first.value;
    }

    /**
     * get last element from list
     *
     * @return last element if there is no elements return null
     */
    public E peekLast() {
        return isEmpty() ? null : last.value;
    }

    /**
     * remove first element
     *
     * @return if there is no elements return null else return first element
     */
    public E removeFirst() {
        if (isEmpty()) {
            return null;
        } else {
            Link<E> temp = first;
            first = first.next;
            size--;
            return temp.value;
        }
    }

    /**
     * remove last element
     *
     * @return last element if there is no elements return null
     */
    public E removeLast() {
        if (isEmpty()) {
            return null;
        } else {
            Link<E> temp = last;
            last = last.previous;
            size--;
            return temp.value;
        }
    }

    /**
     * find the link object by key
     *
     * @param key
     * @return link object
     */
    public Link<E> find(E key) {
        if (isEmpty()) {
            return null;
        } else {
            Link<E> current = first;
            while (!current.value.equals(key)) {
                if (current.next == null) {
                    return null;
                }
                current = current.next;
            }
            return current;
        }
    }

    /**
     * size
     *
     * @return size of linked list
     */
    public int size() {
        return size;
    }

    /**
     * private class link
     *
     * @param <E>
     */
    private class Link<E> {

        //current element key
        private E value;
        //next element
        private Link<E> next;
        //element before
        private Link<E> previous;

        /**
         * initialize the key variable
         *
         * @param value
         */
        Link(E value) {
            this.value = value;
        }

    }
}
