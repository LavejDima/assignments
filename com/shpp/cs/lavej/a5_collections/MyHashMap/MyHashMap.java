package assignments.com.shpp.cs.lavej.a5_collections.MyHashMap;

import java.util.ArrayList;
import java.util.Arrays;

public class MyHashMap<Key, Value> {
//------------------------------------class fields------------------------------    

    private int size;
    private final static int DEFAULT_CAPACITY = 16;
    private Entry<Key, Value>[] table;
    private static final float LOAD_FACTOR = 0.75f;
//------------------------------------Constructor-------------------------------

    /**
     * initialize HashMap. create new Entry table with size of 16
     * elements(DEFAULT_CAPACITY).
     */
    public MyHashMap() {
        this.table = new Entry[DEFAULT_CAPACITY];
    }
//------------------------------------public methods----------------------------

    /**
     * put key-value pair to hash map
     *
     * @param key
     * @param value
     * @return Entry<Key, Value> object
     */
    public Entry<Key, Value> put(Key key, Value value) {
        if (key == null) {
            return putForNullKey(value);
        }
        if (isNeededResize()) {
            resize();
        }
        Entry<Key, Value> newEntry = new Entry(key, value);
        int hash = generateHash(key, table.length);
        if (table[hash] == null) {
            size++;
            return (table[hash] = newEntry);
        }
        Entry<Key, Value> valueFromTable = table[hash];
        if (valueFromTable.key.equals(key)) {
            valueFromTable.setValue(value);
            return valueFromTable;
        }
        while (valueFromTable.next != null) {
            valueFromTable = valueFromTable.next;
            if (valueFromTable.key.equals(key)) {
                valueFromTable.setValue(value);
                return valueFromTable;
            }
        }
        size++;
        return (valueFromTable.next = newEntry);
    }

    /**
     * get element value from hash map by key of element
     *
     * @param key of element
     * @return value pair of key
     */
    public Value get(Key key) {
        if (key == null) {
            return getForNullKey();
        }
        Entry<Key, Value> hashedEntry = table[generateHash(key, table.length)];
        if (hashedEntry == null) {
            return null;
        }
        if (hashedEntry.getNext() == null && hashedEntry.getKey().equals(key)) {
            return hashedEntry.getValue();
        } else {
            while (!(hashedEntry.getNext() == null || hashedEntry.getKey().equals(key))) {
                hashedEntry = hashedEntry.getNext();
            }
            return hashedEntry.getKey().equals(key) ? hashedEntry.getValue() : null;
        }
    }

    /**
     * remove entry object from table by key
     *
     * @param key
     * @return value of removed entry object
     */
    public Value remove(Key key) {
        if (key == null) {
            return removeForNullKey();
        }

        Entry<Key, Value> hashedEntry = table[generateHash(key, table.length)];
        Entry<Key, Value> entry;

        if (hashedEntry == null) {
            return null;
        }
        entry = new Entry<>(hashedEntry.getKey(), hashedEntry.getValue());

        if (entry.getNext() == null && key.equals(entry.getKey())) {
            table[generateHash(key, table.length)] = null;
            size--;
            return entry.getValue();
        }

        while (entry.getNext() != null) {
            if (key.equals(entry.getKey())) {
                hashedEntry.setNext(hashedEntry.getNext().getNext());
                size--;
                return entry.getValue();
            }
            hashedEntry = hashedEntry.getNext();
            entry = entry.getNext();
        }

        if (key.equals(entry.getKey())) {
            hashedEntry = null;
            size--;
            return entry.getValue();
        }

        return null;
    }

    /**
     * size
     *
     * @return number of elements in hash map
     */
    public int size() {
        return size;
    }

    /**
     * is there any elements in hash map
     *
     * @return true if there is no elements
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * returns true if hash map contains element with such key
     *
     * @param key of element
     * @return true if hash map contains element with such key
     */
    public boolean contains(Key key) {
        return getKeyArray().contains(key);
    }

    /**
     * get array list of key
     *
     * @return ArrayList of keys from hash map
     */
    public ArrayList<Key> getKeyArray() {
        ArrayList<Key> keyList = new ArrayList<>();
        for (Entry<Key, Value> entry : getEntryArray()) {
            keyList.add(entry.getKey());
        }
        return keyList;
    }

    /**
     * get array list of values
     *
     * @return array list of values from hash map
     */
    public ArrayList<Value> getValueArray() {
        ArrayList<Value> valueList = new ArrayList<>();
        for (Entry<Key, Value> entry : getEntryArray()) {
            valueList.add(entry.getValue());
        }
        return valueList;
    }

//-----------------------------------private methods----------------------------
    /**
     * hash function
     *
     * @param key needed to built hash code
     * @param tableLength need to compress the hash code returned int size
     * @return hash code
     */
    private int generateHash(Key key, int tableLength) {
        //(number & 0x7fffffff) always returned positive numbers
        //get it (((a * hashcode  + b)& 0x7fffffff) % p) % n from one video
        //a and b is positive numbers
        //n is length of array
        //p is prime that must be larger than table.length
        return (key.hashCode() & 0x7fffffff) % tableLength;
    }

    /**
     * is size of hash map divided by length of table is more than 0.75
     *
     * @return true if number of elements divide by length of table>0.75
     */
    private boolean isNeededResize() {
        return ((double) size() / table.length > LOAD_FACTOR);
    }

    /**
     * resize and rewrite the hash map when load factor is get
     */
    private void resize() {
        Entry<Key, Value>[] newTable = new Entry[table.length * 2 + 1];
        for (Entry<Key, Value> entry : table) {
            if (entry == null) {
                continue;
            } else if (entry.getNext() == null) {
                putToNewTable(newTable, entry);
            } else {
                while (entry.getNext() != null) {
                    putToNewTable(newTable, entry);
                    entry = entry.getNext();
                }
                putToNewTable(newTable, entry);
            }
        }
        table = newTable;

    }

    /**
     * put old values from table to new table, this method is used by resize
     * method
     *
     * @param newTable
     * @param entry
     */
    private void putToNewTable(Entry<Key, Value>[] newTable, Entry<Key, Value> entry) {
        Entry<Key, Value> newEntry = new Entry(entry.getKey(), entry.getValue());
        int hash = generateHash(newEntry.getKey(), newTable.length);
        if (newTable[hash] == null) {
            newTable[hash] = newEntry;
        } else if (newTable[hash].getNext() == null) {
            newTable[hash].setNext(newEntry);
        } else {
            Entry<Key, Value> newTableEntry = newTable[hash];
            while (newTableEntry.getNext() != null) {
                newTableEntry = newTableEntry.getNext();
            }
            newTableEntry.setNext(newEntry);
        }
    }

    /**
     * get array of entry from table ignores null elements
     *
     * @return array of Entry<Key, Value> objects
     */
    private Entry<Key, Value>[] getEntryArray() {
        if (size() == 0) {
            return null;
        }
        Entry<Key, Value>[] arrayOfEntry = new Entry[size()];
        int index = 0;
        for (Entry<Key, Value> entry : table) {
            if (entry == null) {
                continue;
            } else if (entry.getNext() == null) {
                arrayOfEntry[index++] = entry;
            } else {
                while (entry.getNext() != null) {
                    Entry<Key, Value> modifiedEntry = new Entry(entry.getKey(), entry.getValue());
                    modifiedEntry.setNext(null);
                    arrayOfEntry[index++] = modifiedEntry;
                    entry = entry.getNext();
                }
                arrayOfEntry[index++] = entry;
            }
        }
        return arrayOfEntry;
    }

    /**
     * put value to table if key==null
     *
     * @param value
     * @return Entry object
     */
    private Entry<Key, Value> putForNullKey(Value value) {
        Entry<Key, Value> newEntry = new Entry(null, value);
        if (table[0] == null) {
            size++;
            return (table[0] = newEntry);
        }
        Entry<Key, Value> valueFromTable = table[0];
        if (valueFromTable.key == null) {
            valueFromTable.setValue(value);
            return valueFromTable;
        }
        while (valueFromTable.next != null) {
            valueFromTable = valueFromTable.next;
            if (valueFromTable.key == null) {
                valueFromTable.setValue(value);
                return valueFromTable;
            }
        }
        size++;
        return (valueFromTable.next = newEntry);
    }

    /**
     * return value if key is null
     *
     * @return value of null key
     */
    private Value getForNullKey() {
        Entry<Key, Value> entry = table[0];
        if (entry == null) {
            return null;
        }
        if (entry.getNext() == null && entry.getKey() == null) {
            return entry.getValue();
        } else {
            while (!(entry.getNext() == null || entry.getKey() == null)) {
                entry = entry.getNext();
            }
            return entry.getKey() == null ? entry.getValue() : null;
        }
    }

    /**
     * remove entry object for null key
     *
     * @return value of removed object
     */
    private Value removeForNullKey() {
        Entry<Key, Value> entry;
        if ((entry = table[0]) == null) {
            return null;
        }
        entry = new Entry<>(entry.getKey(), entry.getValue());
        if (entry.getNext() == null && entry.getKey() == null) {
            table[0] = null;
            size--;
            return entry.getValue();
        }
        Entry<Key, Value> modifyEntry = table[0];
        while (entry.getNext() != null) {
            if (entry.getKey() == null) {
                modifyEntry.setNext(modifyEntry.getNext().getNext());
                size--;
                return entry.getValue();
            }
            modifyEntry = modifyEntry.getNext();
            entry = entry.getNext();
        }
        if (entry.getKey() == null) {
            modifyEntry = null;
            size--;
            return entry.getValue();
        }
        return null;
    }
//-----------------------------------overrided methods--------------------------

    @Override
    public String toString() {
        String result = "";
        Entry[] arrayOfEntry = getEntryArray();
        if (arrayOfEntry == null) {
            return "[" + result + "]";
        }
        result = String.valueOf(arrayOfEntry[0]);
        for (Entry entry : Arrays.copyOfRange(arrayOfEntry, 1, arrayOfEntry.length)) {
            result += entry != null ? ", " + entry : "";
        }
        return "[" + result + "]";
    }

//-----------------------------------Entry class--------------------------------
    private class Entry<Key, Value> {

        private final Key key;
        private Value value;
        private Entry next;

        public Entry(Key key, Value value) {
            this.key = key;
            this.value = value;
        }

        public Key getKey() {
            return key;
        }

        public Value getValue() {
            return value;
        }

        private Entry getNext() {
            return next;
        }

        private void setNext(Entry next) {
            this.next = next;
        }

        public void setValue(Value value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(key) + ":" + String.valueOf(value);
        }

    }
}
