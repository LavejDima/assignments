package assignments.com.shpp.cs.lavej.a5_collections.MyHeap;

import java.util.Arrays;
import java.util.Comparator;

public class MyHeap<E> {

    private static final int DEFAULT_CAPACITY = 17;
    private int size;
    private Object elementsArray[];
    //comparator object
    private Comparator<? super E> comparator;
//-------------------constructor-----------------------------

    public MyHeap() {
        elementsArray = new Object[DEFAULT_CAPACITY];
    }

    /**
     * constructor with comparator object
     *
     * @param comparator
     */
    public MyHeap(Comparator<? super E> comparator) {
        this.comparator = comparator;
        elementsArray = new Object[DEFAULT_CAPACITY];
    }
//-------------------public methods---------------------------

    /**
     * get the most prioress element end remove it from heap
     *
     * @return most prioress element if there is no elements return null
     */
    public E poll() {
        if (isEmpty()) {
            return null;
        }
        @SuppressWarnings("unchecked")
        E temp = (E) elementsArray[0];
        elementsArray[0] = elementsArray[--size];
        bubbleDown(0);
        return temp;
    }

    /**
     * get the most prioress element and doesn't removes it from heap
     *
     * @return most prioress element if there is no elements return null
     */
    @SuppressWarnings("unchecked")
    public E peek() {
        return isEmpty() ? null : (E) elementsArray[0];
    }

    /**
     * put element to heap
     *
     * @param element
     */
    public void offer(E element) {
        if (size() == elementsArray.length) {
            resize();
        }
        elementsArray[size] = element;
        bubbleUp(size++);
    }

    /**
     * size of heap
     *
     * @return number of elements in heap
     */
    public int size() {
        return size;
    }

    /**
     * is there any element in heap
     *
     * @return true if there is no elements
     */
    public boolean isEmpty() {
        return size() == 0;
    }
//-------------------private methods---------------------------

    /**
     * get index of left child
     *
     * @param indexOfCurrent index of current element
     * @return index of left child
     */
    private int getLeftChildIndex(int indexOfCurrent) {
        return 2 * indexOfCurrent + 1;
    }

    /**
     * get index of right child
     *
     * @param indexOfCurrent index of current element
     * @return index of right child
     */
    private int getRightChildIndex(int indexOfCurrent) {
        return 2 * indexOfCurrent + 2;
    }

    /**
     * get index of parent element
     *
     * @param indexOfCurrent index of current element
     * @return index of parent element
     */
    private int getParentIndex(int indexOfCurrent) {
        return (indexOfCurrent - 1) / 2;
    }

    /**
     * put element from root of pyramid to place where his left and right
     * children's has smaller values
     *
     * @param rootOfHeap
     */
    @SuppressWarnings("unchecked")
    private void bubbleDown(int rootOfHeap) {
        int currentIndex = rootOfHeap;
        Object temp = elementsArray[currentIndex];
        int largerChildIndex;
        while (currentIndex < size() / 2) {
            int leftChildIndex = getLeftChildIndex(currentIndex);
            int rightChildIndex = getRightChildIndex(currentIndex);
            if (comparator == null) {
                if (rightChildIndex < size() && ((Comparable<? super E>) elementsArray[rightChildIndex]).compareTo((E) elementsArray[leftChildIndex]) > 0) {
                    largerChildIndex = rightChildIndex;
                } else {
                    largerChildIndex = leftChildIndex;
                }
                if (((Comparable<? super E>) elementsArray[largerChildIndex]).compareTo(((E) temp)) <= 0) {
                    break;
                }
            } else {
                if (rightChildIndex < size() && (comparator.compare((E) elementsArray[rightChildIndex], (E) elementsArray[leftChildIndex])) > 0) {
                    largerChildIndex = rightChildIndex;
                } else {
                    largerChildIndex = leftChildIndex;
                }
                if (comparator.compare((E) elementsArray[largerChildIndex], (E) temp) <= 0) {
                    break;
                }
            }
            elementsArray[currentIndex] = elementsArray[largerChildIndex];
            currentIndex = largerChildIndex;
        }
        elementsArray[currentIndex] = temp;
    }

    /**
     * push element from right down place of pyramid to place where value of
     * element is smaller than value of his parent
     *
     * @param endValueOffHeapArray
     */
    @SuppressWarnings("unchecked")
    private void bubbleUp(int endValueOffHeapArray) {

        int parentIndex = getParentIndex(endValueOffHeapArray);
        int currentIndex = endValueOffHeapArray;
        E temp = (E) elementsArray[currentIndex];
        if (comparator == null) {
            while (currentIndex > 0 && ((Comparable<? super E>) elementsArray[parentIndex]).compareTo(temp) < 0) {
                elementsArray[currentIndex] = elementsArray[parentIndex];
                currentIndex = parentIndex;
                parentIndex = getParentIndex(currentIndex);
            }
        } else {
            while (currentIndex > 0 && comparator.compare((E) elementsArray[parentIndex], (temp)) < 0) {
                elementsArray[currentIndex] = elementsArray[parentIndex];
                currentIndex = parentIndex;
                parentIndex = getParentIndex(currentIndex);
            }
        }
        elementsArray[currentIndex] = temp;
    }

    /**
     * resize the heap when it needs
     */
    private void resize() {
        elementsArray = Arrays.copyOf(elementsArray, size() * 2);
    }
//----------------------overrided methods-------------------------

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < size(); i++) {
            result += elementsArray[i] + " ";
        }
        return result;
    }

}
