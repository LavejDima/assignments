package assignments.com.shpp.cs.lavej.a5_collections.MyArrayList;

import java.util.Arrays;

public class MyArrayList<E> {

    //maximum array size
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE;
    //default size of array
    private static final int DEFAULT_CAPACITY = 10;
    //array of objects
    private Object[] elementData;
    //number of elements in array
    private int size;

    /**
     * initialize elementData array with size of 10(DEFAULT_CAPACITY)
     */
    public MyArrayList() {
        this.elementData = new Object[DEFAULT_CAPACITY];
    }

    /**
     * get size of array list
     *
     * @return number of elements in array list
     */
    public int size() {
        return size;
    }

    /**
     * check is there any element in array
     *
     * @return true if array is empty
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * check is there such element in array
     *
     * @param element
     * @return true if there is such element in array
     */
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    /**
     * add element to array list if space in element data array is no enough
     * than calls resize method
     *
     * @param element
     * @return true
     */
    public boolean add(E element) {
        if (isFull()) {
            resizeByCoef(2);
        }
        elementData[size++] = element;
        return true;
    }

    /**
     * add element at index position
     *
     * @param index position
     * @param element to add
     */
    public void add(int index, E element) {
        rangeCheck(index);
        if (isFull()) {
            resizeByCoef(2);
        }
        //make hole in index position
        System.arraycopy(elementData, index, elementData, index + 1, size - index);
        //add element
        elementData[index] = element;
        size++;
    }

    /**
     * delete element by index
     *
     * @param index
     * @return specified element that localized by this index
     */
    public E remove(int index) {
        rangeCheck(index);
        //save element from array localized by index
        E element = getElement(index);
        //number of elements to copy
        int numMoved = size - index - 1;
        //if there is elements to move(index isnt last element)
        if (numMoved > 0) {
            //delete bucket in index localization
            System.arraycopy(elementData, index + 1, elementData, index,
                    numMoved);
        }
        //delete last element
        elementData[--size] = null;

        return element;
    }

    /**
     * tell when elementData array must be resized
     *
     * @return true if elementData array must be resized
     */
    private boolean isFull() {
        return size >= elementData.length;
    }

    /**
     * resize the elementData array by Arrays.copyOf method
     */
    private void resizeByCoef(int resizeCoef) {
        int newArraySize = elementData.length * resizeCoef;
        if (newArraySize > MAX_ARRAY_SIZE) {
            throw new OutOfMemoryError();
        }
        elementData = Arrays.copyOf(elementData, newArraySize);
    }

    /**
     * check the range of index if it larger than size or equals size or if it
     * lower than zero method throwing index out of boundary exception
     *
     * @param index
     */
    private void rangeCheck(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("your index " + index + " is larger than number of available indexes of array " + (size() - 1));
        }
        if (index < 0) {
            throw new IndexOutOfBoundsException("index must be larger than zero or equals zero");
        }
    }

    /**
     * makes more simple class cast
     *
     * @param index
     * @return element of E class
     */
    private E getElement(int index) {
        return (E) elementData[index];
    }

    /**
     * get element by index
     *
     * @param index of element that must be returned
     * @return element with current index
     */
    @SuppressWarnings("unchecked")
    public E get(int index) {
        rangeCheck(index);
        return getElement(index);
    }

    /**
     * get index of element
     *
     * @param element
     * @return index of current element
     */
    public int indexOf(E element) {
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elementData[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (element.equals(elementData[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        String result = isEmpty() ? "" : String.valueOf(getElement(0));
        for (int i = 1; i < size(); i++) {
            result += ", " + (getElement(i));
        }
        return "[" + result + "]";
    }

}
