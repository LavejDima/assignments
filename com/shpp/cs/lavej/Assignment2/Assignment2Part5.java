package assignments.com.shpp.cs.lavej.Assignment2;

import acm.graphics.*;
import com.shpp.cs.a.graphics.WindowProgram;
import java.awt.Color;

/*Grid program*/
public class Assignment2Part5 extends WindowProgram {

    /*application constants*/
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 370;

    /* The number of rows and columns in the grid, respectively. */
    private static final int NUM_ROWS = 5;
    private static final int NUM_COLS = 6;

    /* The width and height of each box. */
    private static final double BOX_SIZE = 40;

    /* The horizontal and vertical spacing between the boxes. */
    private static final double BOX_SPACING = 10;

    /*method draws the rectangle, return rectangle*/
    private GRect drawRectangle(double x, double y, double width, double height, Color color) {
        GRect rectangle = new GRect(x, y, width, height);
        rectangle.setFillColor(color);
        rectangle.setFilled(true);
        add(rectangle);
        return rectangle;

    }

    /*method draw grid from rectangles which creates by calling drawRectangle in loop*/
    private void drawGrid() {
        double distance = BOX_SIZE + BOX_SPACING;

        for (int n = 0; n <= NUM_ROWS; n++) {
            for (int i = 0; i <= NUM_COLS; i++) {
                drawRectangle((distance * i) + 30, (distance * n) + 30, BOX_SIZE, BOX_SIZE, Color.black);
            }

        }

    }

    public void run() {

        drawGrid();

    }
}
