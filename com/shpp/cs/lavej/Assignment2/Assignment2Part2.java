package assignments.com.shpp.cs.lavej.Assignment2;

import acm.graphics.*;
import com.shpp.cs.a.graphics.WindowProgram;
import java.awt.Color;

/*white box in black circles program*/
public class Assignment2Part2 extends WindowProgram {

    /*application parameters of size*/
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 400;

    /*method draws circles*/
    private void drawCenteredCircle(double xCenter, double yCenter,
            double radius, Color color) {
        /* Compute the coordinates of the upper-left corner of the circle,
         * which we need when we create the oval, from the center and radius.  
         */
        double x = xCenter - radius;
        double y = yCenter - radius;
        GOval circle = new GOval(x, y, 2 * radius, 2 * radius);
        circle.setColor(color);
        circle.setFilled(true);
        add(circle);
    }

    /*method draws rectangles*/
    private void drawRectangle(double x, double y, double width, double height, Color color) {
        double xCentRect = x - (width / 2);
        double yCentRect = y - (height / 2);
        GRect rect = new GRect(xCentRect, yCentRect, width, height);
        rect.setFilled(true);
        rect.setColor(color);
        add(rect);

    }

    public void run() {

        /*get the centers coordinates, taking constants works correctly*/
        double centerX = APPLICATION_WIDTH / 2.0;
        double centerY = APPLICATION_HEIGHT / 2.0;
        /*instead taking value from getWidth() or getHeight()*/
        //double centerX = getWidth() / 2.0;
        //double centerY = getHeight() / 2.0;
        /*draw the figures, by calling methods*/
        drawCenteredCircle(centerX / 2, centerY / 2, getWidth() / 6, Color.black);
        drawCenteredCircle(centerX / 2 + centerX, centerY / 2, getWidth() / 6, Color.black);
        drawCenteredCircle(centerX / 2, centerY / 2 + centerY, getWidth() / 6, Color.black);
        drawCenteredCircle(centerX / 2 + centerX, centerY / 2 + centerY, getWidth() / 6, Color.black);
        drawRectangle(centerX, centerY, getHeight() / 2, getWidth() / 2, Color.white);

    }
}
