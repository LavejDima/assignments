package assignments.com.shpp.cs.lavej.Assignment2;

import acm.graphics.*;
import acm.graphics.GLabel;
import com.shpp.cs.a.graphics.WindowProgram;
import java.awt.Color;

/*Flag program*/
public class Assignment2Part4 extends WindowProgram {

    /*application constants*/
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 400;
    /*name of flag*/
    private final static String NAME_OF_FLAG = "The Belgium Flag";
    /*flag colors*/
    private static final Color RED = new Color(255, 0, 0);
    private static final Color YELLOW = new Color(255, 255, 0);
    private static final Color BLACK = new Color(0, 0, 0);

    /*method draw the rectangle*/
    private void drawRectangle(double x, double y, double width, double height, Color color) {
        GRect rectangle = new GRect(x, y, width, height);
        rectangle.setFillColor(color);
        rectangle.setFilled(true);
        add(rectangle);

    }

    /*method draw flag by calling drawRectangle several times*/
    private void drawFlag() {
        drawRectangle(15, 10, (getWidth() / 3) - 10, getHeight() - 40, BLACK);
        drawRectangle(getWidth() / 3, 10,
                (getWidth() / 3) - 10, getHeight() - 40, YELLOW);
        drawRectangle((getWidth() / 3 + getWidth() / 3) - 15, 10,
                (getWidth() / 3) - 10, getHeight() - 40, RED);

    }

    /*method wich prepare flag name, takes sting as a parametr*/
    private void nameOfFlag(String nameOfFlag) {
        GLabel name = new GLabel(nameOfFlag);
        name.setFont("Times-12");
        name.setColor(Color.black);
        name.setLocation(getWidth() - name.getWidth() - 10, getHeight() - name.getHeight());
        add(name);
    }

    public void run() {
        drawFlag();
        nameOfFlag(NAME_OF_FLAG);

    }
}
