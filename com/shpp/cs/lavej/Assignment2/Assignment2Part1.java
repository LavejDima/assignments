package assignments.com.shpp.cs.lavej.Assignment2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*The square root program*/
public class Assignment2Part1 {

    private double a = 0;
    private double b = 0;
    private double c = 0;
    private double x1 = 0;
    private double x2 = 0;
    private double discriminant = 0;
    private Integer n = 0;
    private double dCoef;

    /*solves the discriminant*/
    private double solveDiscriminant() {
        discriminant = (b * b) - ((4 * a) * c);
        return discriminant;
    }

    /*checs the solveDiscriminant() for values and return int */
    private int checkDiscriminant() {
        if (solveDiscriminant() > 0) {
            n = 2;
        } else if (solveDiscriminant() == 0) {
            n = 1;
        }
        if (solveDiscriminant() < 0) {
            n = 0;
        }

        return n;

    }

    /*method solve the square root, and check 
    by method checkDiscriminant() to know how much roots are*/
    private void solveSqRoot() {
        if (checkDiscriminant() == 2) {
            x1 = (-b + solveDiscriminant()) / (2 * a);
            x2 = (-b - solveDiscriminant()) / (2 * a);

            System.out.print("There are two roots: ");
            System.out.print(x1);
            System.out.print(" and ");
            System.out.print(x2);

        } else if (checkDiscriminant() == 1) {
            x1 = (-b + solveDiscriminant()) / (2 * a);

            System.out.print("There is one root: ");
            System.out.print(x1);

        } else if (checkDiscriminant() == 0) {
            System.out.print("There are no real roots.");
        }

    }

    /*method take double from console and return it to program*/
    private double getCoef() {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            dCoef = Double.parseDouble(reader.readLine());

        } catch (NumberFormatException | IOException e) {

            System.out.println("incorrect number");
        }
        return dCoef;

    }

    /*method print text to console and take values from getCoef() method
    and store it to variables*/
    private void initCoef() {
        System.out.print("Please enter a: ");
        a = getCoef();

        if (a == 0.0) {

            while (a == 0.0) {
                System.out.println("Please enter \"a\" that differs from 0");
                System.out.println("Please enter a: ");
                a = getCoef();

            }

        }
        System.out.print("Please enter b: ");
        b = getCoef();

        System.out.print("Please enter c: ");
        c = getCoef();

    }

    public static void main(String[] args) {
        Assignment2Part1 SqRoot = new Assignment2Part1();
        SqRoot.initCoef();
        SqRoot.solveDiscriminant();
        SqRoot.checkDiscriminant();
        SqRoot.solveSqRoot();

    }

}
