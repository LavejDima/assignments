package assignments.com.shpp.cs.lavej.Assignment2;

import com.shpp.cs.a.graphics.WindowProgram;
import acm.graphics.*;
import java.awt.Color;

/*Caterpillar program*/
public class Assignment2Part6 extends WindowProgram {

    /*application constants*/
    public static final int APPLICATION_WIDTH = 600;
    public static final int APPLICATION_HEIGHT = 100;
    /*caterpillar constants of segments*/
    public static final int SEGMENT_X_OFFSET = 35;
    public static final int SEGMENT_Y_OFFSET = 10;
    public static final int SEGMENT_HEIGHT = 50;
    public static final int SEGMENT_WIDTH = 50;
    /*default value of numberOfSegments*/
    public int numberOfSegments = 5;
    /*color of segments and its conturs*/
    private static final Color GREEN = new Color(0, 255, 0);
    private static final Color RED = new Color(255, 0, 0);

    /*method take input value of int from console
    and paste it to variable numberOfSegments*/
    private void setNumberOfSegments() {
        int number = readInt("write number of segments: ");
        numberOfSegments = number;

    }

    /*method draws the circle*/
    private void drawSegment(double x, double y, int width, int height) {
        GOval segment = new GOval(x, y, width, height);
        /*takes colors from constants*/
        segment.setFillColor(GREEN);
        segment.setFilled(true);
        segment.setColor(RED);
        add(segment);
    }

    /*method draws the catterpillar*/
    private void drawCaterpillar() {
        for (int i = 1; i <= numberOfSegments; i++) {
            if (i % 2 == 0) {
                drawSegment(SEGMENT_X_OFFSET * i, SEGMENT_Y_OFFSET + 15,
                        SEGMENT_WIDTH, SEGMENT_HEIGHT);
            } else {
                drawSegment(SEGMENT_X_OFFSET * i, SEGMENT_Y_OFFSET,
                        SEGMENT_WIDTH, SEGMENT_HEIGHT);
            }

        }

    }

    public void run() {
        /*set number of segments*/
        setNumberOfSegments();
        /*draw caterpillar*/
        drawCaterpillar();

    }

}
