package assignments.com.shpp.cs.lavej.Assignment6.sg;

import acm.graphics.*;

public class SteganographyLogic {

    /**
     * Given a GImage containing a hidden message, finds the hidden message
     * contained within it and returns a boolean array containing that message.
     * <p/>
     * A message has been hidden in the input image as follows. For each pixel
     * in the image, if that pixel has a red component that is an even number,
     * the message value at that pixel is false. If the red component is an odd
     * number, the message value at that pixel is true.
     *
     * @param source The image containing the hidden message.
     * @return The hidden message, expressed as a boolean array.
     */
    public static boolean[][] findMessage(GImage source) {
        int[][] pixelArray = source.getPixelArray();
        boolean[][] whereIsOdd = new boolean[pixelArray.length][pixelArray[0].length];
        for (int i = 0; i < pixelArray.length; i++) {
            for (int n = 0; n < pixelArray[i].length; n++) {
                if ((GImage.getRed(pixelArray[i][n])) % 2 != 0) {
                    whereIsOdd[i][n] = true;
                } else {
                    whereIsOdd[i][n] = false;
                }
            }
        }
        return whereIsOdd;
    }

    /**
     * Hides the given message inside the specified image.
     * <p/>
     * The image will be given to you as a GImage of some size, and the message
     * will be specified as a boolean array of pixels, where each white pixel is
     * denoted false and each black pixel is denoted true.
     * <p/>
     * The message should be hidden in the image by adjusting the red channel of
     * all the pixels in the original image. For each pixel in the original
     * image, you should make the red channel an even number if the message
     * color is white at that position, and odd otherwise.
     * <p/>
     * You can assume that the dimensions of the message and the image are the
     * same.
     * <p/>
     *
     * @param message The message to hide.
     * @param source The source image.
     * @return A GImage whose pixels have the message hidden within it.
     */
    public static GImage hideMessage(boolean[][] message, GImage source) {
        int[][] pixelArray = source.getPixelArray();
        for (int i = 0; i < message.length; i++) {
            for (int n = 0; n < message[i].length; n++) {
                int redComponent = GImage.getRed(pixelArray[i][n]);
                int greenComponent = GImage.getGreen(pixelArray[i][n]);
                int blueComponent = GImage.getBlue(pixelArray[i][n]);
                if (redComponent % 2 != 0 && redComponent != 0) {
                    pixelArray[i][n] = redComponent < 255
                            ? GImage.createRGBPixel(redComponent + 1,
                                    greenComponent,
                                    blueComponent)
                            : GImage.createRGBPixel(redComponent - 1,
                                    greenComponent,
                                    blueComponent);
                }
                if (message[i][n]) {
                    pixelArray[i][n] = redComponent < 255
                            ? GImage.createRGBPixel(redComponent + 1,
                                    greenComponent,
                                    blueComponent)
                            : GImage.createRGBPixel(redComponent,
                                    greenComponent,
                                    blueComponent);
                    if (GImage.getRed(pixelArray[i][n]) % 2 == 0) {
                        pixelArray[i][n] = GImage.createRGBPixel(redComponent,
                                greenComponent,
                                blueComponent);
                    }
                }
            }
        }
        return new GImage(pixelArray);
    }
}
