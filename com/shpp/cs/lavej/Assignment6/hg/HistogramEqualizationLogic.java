package assignments.com.shpp.cs.lavej.Assignment6.hg;

import acm.graphics.GImage;

public class HistogramEqualizationLogic {

    private static final int MAX_LUMINANCE = 255;

    /**
     * Given the luminances of the pixels in an image, returns a histogram of
     * the frequencies of those luminances.
     * <p/>
     * You can assume that pixel luminances range from 0 to MAX_LUMINANCE,
     * inclusive.
     *
     * @param luminances The luminances in the picture.
     * @return A histogram of those luminances.
     */
    public static int[] histogramFor(int[][] luminances) {
        int[] histogramFor = new int[MAX_LUMINANCE + 1];
        for (int i = 0; i < luminances.length; i++) {
            for (int n = 0; n < luminances[i].length; n++) {
                histogramFor[GImage.getBlue(luminances[i][n])]
                        = histogramFor[GImage.getBlue(luminances[i][n])] + 1;
            }
        }
        return histogramFor;
    }

    /**
     * Given a histogram of the luminances in an image, returns an array of the
     * cumulative frequencies of that image. Each entry of this array should be
     * equal to the sum of all the array entries up to and including its index
     * in the input histogram array.
     * <p/>
     * For example, given the array [1, 2, 3, 4, 5], the result should be [1, 3,
     * 6, 10, 15].
     *
     * @param histogram The input histogram.
     * @return The cumulative frequency array.
     */
    public static int[] cumulativeSumFor(int[] histogram) {
        int[] cumulativeSumFor = histogram;
        for (int i = 0; i < cumulativeSumFor.length; i++) {
            if (i != 0) {
                cumulativeSumFor[i] = cumulativeSumFor[i - 1] + cumulativeSumFor[i];
            } else {
                cumulativeSumFor[i] = histogram[i];
            }
        }
        return cumulativeSumFor;
    }

    /**
     * Returns the total number of pixels in the given image.
     *
     * @param luminances A matrix of the luminances within an image.
     * @return The total number of pixels in that image.
     */
    public static int totalPixelsIn(int[][] luminances) {
        int totalPixelsIn = 0;
        for (int i = 0; i < luminances.length; i++) {
            for (int n = 0; n < luminances[i].length; n++) {
                totalPixelsIn++;
            }
        }
        return totalPixelsIn;
    }

    /**
     * Applies the histogram equalization algorithm to the given image,
     * represented by a matrix of its luminances.
     * <p/>
     * You are strongly encouraged to use the three methods you have implemented
     * above in order to implement this method.
     *
     * @param luminances The luminances of the input image.
     * @return The luminances of the image formed by applying histogram
     * equalization.
     */
    public static int[][] equalize(int[][] luminances) {
        //System.out.println("luminances length before for "+luminances.length);
        //System.out.println("luminances[0] length before for "+luminances[0].length);
        double fractionSmaller = 0.0;
        int[] newLuminance = new int[256];
        int[] cumulativeHistogram = cumulativeSumFor(histogramFor(luminances));
        int totalPixels = totalPixelsIn(luminances);
        for (int n = 0; n < 256; n++) {
            fractionSmaller = cumulativeHistogram[n] / (totalPixels + 0.0);
            newLuminance[n] = (int) (MAX_LUMINANCE * fractionSmaller);
        }
        for (int i = 0; i < luminances.length; i++) {
            for (int n = 0; n < luminances[i].length; n++) {
                int blueComponent = newLuminance[GImage.getBlue(luminances[i][n])];
                luminances[i][n] = blueComponent;
            }
        }
        //System.out.println("luminances length after for "+luminances.length);
        //System.out.println("luminances[0] length after for "+luminances[0].length);
        return luminances;
    }
}
