package assignments.com.shpp.cs.lavej.Assignment6.tm;

public class ToneMatrixLogic {

    /**
     * Given the contents of the tone matrix, returns a string of notes that
     * should be played to represent that matrix.
     *
     * @param toneMatrix The contents of the tone matrix.
     * @param column The column number that is currently being played.
     * @param samples The sound samples associated with each row.
     * @return A sound sample corresponding to all notes currently being played.
     */
    public static double[] matrixToMusic(boolean[][] toneMatrix, int column, double[][] samples) {

        double[] result = new double[ToneMatrixConstants.sampleSize()];
        double maxValue = 0.0;

        for (int y = 0; y < toneMatrix.length; y++) {
            //check correct column
            if (toneMatrix[y][column]) {
                //put samples into result array
                for (int x = 0; x < samples[y].length; x++) {
                    result[x] += samples[y][x];
                }
            }
        }

        //finding max value in result array
        for (int i = 0; i < result.length; i++) {
            maxValue = (Math.abs(result[i]) > maxValue) ? Math.abs(result[i]) : maxValue;
        }

        //dividing all values on max value
        for (int i = 0; i < result.length; i++) {
            if (result[i] != 0) {
                result[i] /= maxValue;
            }
        }

        return result;
    }
}
