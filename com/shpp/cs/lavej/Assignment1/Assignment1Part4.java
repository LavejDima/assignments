package assignments.com.shpp.cs.lavej.Assignment1;

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part4 extends KarelTheRobot {

    /*
          gives new functionality of turning right to Karel
     */
    private void turnRight() throws Exception {
        turnLeft();
        turnLeft();
        turnLeft();
    }

    /*
         method that fills stroke by beepers through a single cell
     */
    private void fillStroke() throws Exception {
        putBeeper();
        while (frontIsClear()) {

            move();
            if (frontIsClear()) {
                move();
                putBeeper();
            }

        }
    }

    /*
         method that invokes method fillStroke() and fill the field - stroke by
         stroke, from down to up, method detects the state of Karel, and works
         specifically, based on the conditions
     */
    private void checkerBoardBuilder() throws Exception {
        while (frontIsClear()) {
            if (facingEast()) {
                fillStroke();
                turnLeft();

                if (frontIsClear() && beepersPresent()) {
                    move();
                    turnLeft();
                    move();
                } else if (frontIsClear() && noBeepersPresent()) {
                    move();
                    turnLeft();
                }
            } else if (facingWest()) {
                fillStroke();
                turnRight();
                if (frontIsClear()) {
                    move();
                    turnRight();
                }
            }
        }
    }

    public void run() throws Exception {
        checkerBoardBuilder();

    }

}
