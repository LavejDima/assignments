package assignments.com.shpp.cs.lavej.Assignment1;

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part3 extends KarelTheRobot {

    /*
	 gives new functionality of turning around to Karel
     */
    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }

    /*
	  method that fills stroke fully by beepers
     */
    private void fillStroke() throws Exception {
        putBeeper();
        while (frontIsClear()) {
            move();
            putBeeper();
        }

    }

    /*
	  method remove beepers from extreme east and extreme west points, which
	  locates abut at the walls
     */
    private void pickBeepersFromEnds() throws Exception {
        turnAround();
        pickBeeper();
        while (frontIsClear()) {
            move();
        }
        pickBeeper();
        turnAround();
        move();
    }

    /*
	  method remove beepers from extreme points, Karel go to the middle from
	  ends and delete consequentially beepers and the last beeper that will be
	  deleted is middle beeper
     */
    private void findMidPoint() throws Exception {
        pickBeeper();
        move();
        while (beepersPresent()) {
            move();
        }
        turnAround();
        move();

    }

    public void run() throws Exception {
        /*
             this condition will works, when level will consist only from one
             column, or single cell stroke
         */
        if (frontIsBlocked()) {
            putBeeper();
            /*
		 if there where more that one column, or single cell stroke in
		 level, then this part of code will be performed
             */
        } else if (frontIsClear()) {
            fillStroke();
            pickBeepersFromEnds();
            while (beepersPresent()) {
                findMidPoint();
            }
            /*
		 put beeper in the place where the last - middle beeper was
		 deleted
             */
            putBeeper();
        }
    }

}
