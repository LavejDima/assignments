package assignments.com.shpp.cs.lavej.Assignment1;

import com.shpp.karel.KarelTheRobot;

/*Column builder Karel*/

 /*
     methods of this class works correctly only if complied start conditions
     that are:
     - Karel stays in extreme west-south points of scene (left-down point) 
     - Karel is facing east 
 */
public class Assignment1Part2 extends KarelTheRobot {

    /*
	  gives new functionality of turning right to Karel
     */
    private void turnRight() throws Exception {
        turnLeft();
        turnLeft();
        turnLeft();
    }

    /*
	  method of moving between the columns, gives moves from one column to next
	  detects the state of Karel, and works specifically, based on the
	  conditions
     */
    private void goToNextColumn() throws Exception {
        if (facingNorth() && frontIsBlocked()) {
            turnRight();
            move();
            move();
            move();
            move();

        } else if (facingSouth() && frontIsClear()) {
            turnLeft();
            move();
            move();
            move();
            move();

        } else if (facingEast() && frontIsClear()) {

            move();
            move();
            move();
            move();

        }
    }

    /*
	  method of building columns detects the state of Karel, and works
	  specifically, based on the conditions
     */
    private void buildColumn() throws Exception {
        if (rightIsBlocked()) {
            turnLeft();
            if (beepersPresent()) {
                move();
            } else if (noBeepersPresent()) {
                putBeeper();

            }
            while (frontIsClear()) {
                if (beepersPresent()) {
                    move();
                } else if (noBeepersPresent()) {

                    move();
                    putBeeper();

                }

                if (noBeepersPresent()) {
                    putBeeper();
                }
            }
            turnRight();

        } else if (leftIsBlocked()) {
            turnRight();
            if (beepersPresent()) {
                move();
            } else if (noBeepersPresent()) {
                putBeeper();

            }
            while (frontIsClear()) {
                if (beepersPresent()) {
                    move();
                } else if (noBeepersPresent()) {
                    putBeeper();
                    move();

                }

                if (noBeepersPresent()) {
                    putBeeper();
                }
            }
            turnLeft();

        }
    }

    public void run() throws Exception {

        /*
             in if-else state we detect the start conditions of the level, and
             based on them our actions
         */
        if (frontIsBlocked()) {
            buildColumn();
        } else if (frontIsClear()) {
            buildColumn();
            while (frontIsClear()) {
                goToNextColumn();
                buildColumn();
            }

        }
    }

}
