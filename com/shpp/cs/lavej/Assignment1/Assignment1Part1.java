package assignments.com.shpp.cs.lavej.Assignment1;

import com.shpp.karel.KarelTheRobot;

/*Pick paper chalenge*/
public class Assignment1Part1 extends KarelTheRobot {

    /*
	  method make Karel move until front is clear
	  
     */
    private void goIfNoLimitYourFront() throws Exception {
        while (frontIsClear()) {
            move();
        }
    }

    /*
	  gives new functionality of turning right to Karel
     */
    private void turnRight() throws Exception {
        turnLeft();
        turnLeft();
        turnLeft();
    }

    /*
	  gives new functionality of turning around to Karel
     */
    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();

    }

    /*
	  method describes the sort of Karels moves to the beeper
     */
    private void goToPaper() throws Exception {
        goIfNoLimitYourFront();
        turnRight();
        move();
        turnLeft();
        while (noBeepersPresent()) {
            move();
        }

    }

    /*
	  method describes to Karel take beeper, then rotate 180 degrees
     */
    private void takePaper() throws Exception {
        pickBeeper();
        turnAround();
    }

    /*
	  method describes the sort of Karels moves back from the place of beeper
	  location
     */
    private void goBack() throws Exception {
        goIfNoLimitYourFront();
        turnRight();
        goIfNoLimitYourFront();
        turnAround();

    }

    public void run() throws Exception {
        goToPaper();
        takePaper();
        goBack();
    }
}
