package assignments.com.shpp.cs.lavej.a2_silhouettes.Silhouettes;

public class PixelGraph {

    private final Point point;
    private boolean visited;
    private Point[] neighborsLocation;

    /**
     * Constructor of pixelGraph class
     * <br> initializing the variables here
     *
     * @param xLocation of pixel
     * @param yLocation of pixel
     * @param numberOfNeighbours number of pixels thats lying near
     */
    PixelGraph(Point point, Point[] neighbors) {
        this.neighborsLocation = neighbors;
        this.point = point;
    }

    /**
     * is this point visited before
     *
     * @return if i was here return true, else false
     */
    public boolean isVisited() {
        return this.visited;
    }

    /**
     * set this point as visited
     *
     * @param visited
     */
    public void setVisit(boolean visited) {
        this.visited = visited;
    }

    /**
     * return Point object, witch has x and y variables
     *
     * @return Point object
     */
    public Point getPoint() {
        return this.point;
    }

    /**
     * get value of pixel
     *
     * @return blue component of pixel in original image
     */
    public boolean getValueOfPixel() {
        return this.point.getValueOfPoint();
    }

    /**
     * set value of pixel
     *
     * @param value is pixel black
     */
    public void setValueOfPixel(boolean value) {
        point.setValueOfPoint(value);
    }

    /**
     * get location of neighbors
     *
     * @return arrayList of points
     */
    public Point[] getNeighborsLocation() {
        return this.neighborsLocation;
    }

    @Override
    public String toString() {
        return "point - " + getPoint() + "; " + " isWasHere- " + isVisited() + "; value of pixel- " + getValueOfPixel();
    }

}
