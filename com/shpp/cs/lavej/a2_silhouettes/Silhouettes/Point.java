package assignments.com.shpp.cs.lavej.a2_silhouettes.Silhouettes;

public class Point {

    //location of point
    private final int xLocation;
    private final int yLocation;
    private boolean value;

    /**
     * initializing the location of point in constructor
     *
     * @param x
     * @param y
     */
    public Point(int x, int y, boolean value) {
        this.xLocation = x;
        this.yLocation = y;
        this.value = value;
    }

    /**
     * get x location of point
     *
     * @return location on x
     */
    public int getX() {
        return this.xLocation;
    }

    /**
     * get y location of point
     *
     * @return location on y
     */
    public int getY() {
        return this.yLocation;
    }

    public boolean getValueOfPoint() {
        return this.value;
    }

    public void setValueOfPoint(boolean value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "x - " + getX() + " , y - " + getY();
    }

    @Override
    public boolean equals(Object obj) {
        Point point = (Point) obj;
        if (point.getX() == this.getX()
                && point.getY() == this.getY()) {
            return true;
        }
        return false;
    }

}
