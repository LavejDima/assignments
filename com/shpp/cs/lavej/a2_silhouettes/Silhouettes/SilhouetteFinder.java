package assignments.com.shpp.cs.lavej.a2_silhouettes.Silhouettes;

import assignments.com.shpp.cs.lavej.a2_silhouettes.Collections.MyDeque;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import javax.imageio.ImageIO;

public class SilhouetteFinder {

    public static void main(String[] args) {
        new SilhouetteFinder().run(new File(args[0]));
    }

    public SilhouetteFinder() {

    }

    private void run(File fileName) {
        System.out.println("number of silhouettes " + findSilhouettes(getPixelGraph(file2Bitmap(fileName))));
    }

    /**
     * get boolean[][] from image
     *
     * @param fileName
     * @return boolean[][]
     */
    private boolean[][] file2Bitmap(File fileName) {
        try {
            BufferedImage img = ImageIO.read(new FileInputStream(fileName));
            int width = img.getWidth();
            int height = img.getHeight();
            boolean[][] pixels = new boolean[height][width];
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    pixels[y][x] = new Color(img.getRGB(x, y)).getBlue() <= 50;
                }
            }
            return pixels;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * get neighbors location
     *
     * @param image
     * @param xLocation
     * @param yLocation
     * @return array of points
     */
    private Point[] getNeighborsLocation(boolean[][] image, int xLocation, int yLocation) {
        Point[] neighborsArray = new Point[5];

        boolean isLeftBlocked = xLocation == 0;
        boolean isRightBlocked = xLocation == image.length - 1;
        boolean isTopBlocked = yLocation == 0;
        boolean isDownBlocked = yLocation == image[0].length - 1;

        neighborsArray[0] = createNewPoint(xLocation, yLocation, image);
        neighborsArray[1] = isTopBlocked ? null : createNewPoint(xLocation, yLocation - 1, image);
        neighborsArray[2] = isRightBlocked ? null : createNewPoint(xLocation + 1, yLocation, image);
        neighborsArray[3] = isLeftBlocked ? null : createNewPoint(xLocation - 1, yLocation, image);
        neighborsArray[4] = isDownBlocked ? null : createNewPoint(xLocation, yLocation + 1, image);
        return neighborsArray;
    }

    /**
     * get pixelGraph[][] from boolean[][]
     *
     * @param image
     * @return pixelGraph[][]
     */
    private PixelGraph[][] getPixelGraph(boolean[][] image) {
        PixelGraph[][] pixelGraph = new PixelGraph[image.length][image[0].length];
        for (int y = 0; y < image.length; y++) {
            for (int x = 0; x < image[0].length; x++) {
                Point[] neighbors = getNeighborsLocation(image, y, x);
                pixelGraph[y][x] = new PixelGraph(neighbors[0], neighbors);
            }
        }
        return pixelGraph;
    }

    /**
     * find silhouettes in pixelGraph[][]
     *
     * @param graph
     */
    private int findSilhouettes(PixelGraph[][] graph) {
        int numberOfSilhouettes=0;
        for (int y = 0; y < graph.length; y++) {
            for (int x = 0; x < graph[y].length; x++) {
                PixelGraph pixelGraph = graph[y][x];
                if (pixelGraph.getValueOfPixel() && !pixelGraph.isVisited()) {
                    bfsBlack(graph, new Point(y, x, pixelGraph.getPoint().getValueOfPoint()));
                    numberOfSilhouettes++;
                }
            }
        }
        return numberOfSilhouettes;
    }

    /**
     * bfs only in black pixels of PixelGraph[][]
     *
     * @param pixelGraph
     * @param point start point
     */
    private void bfsBlack(PixelGraph[][] pixelGraph, Point point) {
        MyDeque<PixelGraph> dequeBlack = new MyDeque<>();
        dequeBlack.putLast(pixelGraph[point.getX()][point.getY()]);
        while (!dequeBlack.isEmpty()) {
            dequeBlack.peekFirst().setVisit(true);
//                System.out.println(dequeBlack.peekFirst());
            for (Point link : dequeBlack.pollFirst().getNeighborsLocation()) {
                if (!(link == null || (dequeBlack.contains(pixelGraph[link.getX()][link.getY()]))
                        || pixelGraph[link.getX()][link.getY()].isVisited())
                        && pixelGraph[link.getX()][link.getY()].getValueOfPixel()) {
                    dequeBlack.putLast(pixelGraph[link.getX()][link.getY()]);
                }
            }
        }
    }

    private Point createNewPoint(int xLocation, int yLocation, boolean[][] image) {
        return new Point(xLocation, yLocation, image[xLocation][yLocation]);
    }

}
