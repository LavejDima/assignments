package assignments.com.shpp.cs.lavej.a2_silhouettes.Collections;

public class MyLinkedList<E> {

    private Link<E> first;
    private Link<E> last;
    private int size;

    public MyLinkedList() {
        this.first = null;
        this.last = null;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public void putFirst(E element) {
        Link<E> newLink = new Link<>(element);
        if (isEmpty()) {
            last = newLink;
        } else {
            first.previous = newLink;
            newLink.next = first;
        }
        first = newLink;
        size++;
    }

    public void putLast(E element) {
        Link<E> newLink = new Link<>(element);
        if (isEmpty()) {
            first = newLink;
        } else {
            last.next = newLink;
            newLink.previous = last;
        }
        last = newLink;
        size++;
    }

    public E peekFirst() {
        return first.key;
    }

    public E peekLast() {
        return last.key;
    }

    public E removeFirst() {
        if (isEmpty()) {
            return null;
        } else {
            Link<E> temp = first;
            first = first.next;
            size--;
            return temp.key;
        }
    }

    public E removeLast() {
        if (isEmpty()) {
            return null;
        } else {
            Link<E> temp = last;
            last = last.previous;
            size--;
            return temp.key;
        }
    }

    public Link<E> find(E key) {
        if (isEmpty()) {
            return null;
        } else {
            Link<E> current = first;
            while (!current.key.equals(key)) {
                if (current == null || current.next == null) {
                    return null;
                }
                current = current.next;
            }
            return current;
        }
    }

    public int size() {
        return size;
    }

    private class Link<E> {

        private E key;
        private Link<E> next;
        private Link<E> previous;

        Link(E key) {
            this.key = key;
        }

    }
}
