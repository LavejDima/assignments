package assignments.com.shpp.cs.lavej.a2_silhouettes.Collections;

public class MyDeque<E> {

    private MyLinkedList<E> linkedList;

    public MyDeque() {
        linkedList = new MyLinkedList<>();
    }

    public void putLast(E element) {
        linkedList.putLast(element);
    }

    public void putFirst(E element) {
        linkedList.putFirst(element);
    }

    public E peekFirst() {
        return linkedList.peekFirst();
    }

    public E peekLast() {
        return linkedList.peekLast();
    }

    public E pollFirst() {
        return linkedList.removeFirst();
    }

    public E pollLast() {
        return linkedList.removeLast();
    }

    public int size() {
        return linkedList.size();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean contains(E key) {
        return linkedList.find(key) != null;
    }
}
