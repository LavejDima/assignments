package assignments.com.shpp.cs.lavej.Assignment7;

/*
 * File: NameSurfer.java
 * ---------------------
 * When it is finished, this program will implements the viewer for
 * the baby-name database described in the assignment handout.
 */
import acm.program.*;
import com.shpp.cs.a.simple.SimpleProgram;
import java.awt.Event;

import java.awt.event.*;
import javax.swing.*;

public class NameSurfer extends SimpleProgram implements NameSurferConstants {

    /**
     * naming before textfield
     */
    private JLabel labelName;
    /**
     * field to print text in
     */
    private JTextField printedNameTextField;
    /**
     * buttons
     */
    private JButton graphButton;
    private JButton clearButton;
    /**
     * stored values from data base
     */
    private NameSurferDataBase dataBase;
    /**
     * draw graphic representation of dependencies
     */
    private NameSurferGraph graphicRepresenrarion;

    /**
     * create Graph Button locate it north
     */
    private void createGraphButton() {
        graphButton = new JButton("Graph");
        graphButton.addActionListener(new GraphButtonPress());
        add(graphButton, NORTH);
    }

    /**
     * create Clear Button locate it north
     */
    private void createClearButton() {
        clearButton = new JButton("Clear");
        clearButton.addActionListener(new ClearButtonPress());
        add(clearButton, NORTH);
    }

    /**
     * create Label Name locate it north
     */
    private void createLabelName() {
        labelName = new JLabel("Name");
        add(labelName, NORTH);
    }

    /**
     * create Graphic Representation of dependencies
     *
     */
    private void createGraphicRepresentation() {
        graphicRepresenrarion = new NameSurferGraph();
        add(graphicRepresenrarion);
    }

    /**
     * create TextField for text print in
     */
    private void createPrintedNameTextField() {
        printedNameTextField = new JTextField();
        printedNameTextField.setColumns(10);
        printedNameTextField.addKeyListener(new EnterPressed());
        add(printedNameTextField, NORTH);
    }

    /**
     * create Data Base of values and names from file
     */
    private void createDataBase() {
        dataBase = new NameSurferDataBase(NAMES_DATA_FILE);
    }

    /* Method: init() */
    /**
     * This method has the responsibility for reading in the data base and
     * initializing the interactors at the top of the window.
     */
    public void init() {
        createDataBase();
        createLabelName();
        createPrintedNameTextField();
        createGraphButton();
        createClearButton();
        createGraphicRepresentation();
    }

    /**
     * update Graphic Representation by calling update() from NameSurferGraph
     */
    private void updateGraphicRepresentation() {
        graphicRepresenrarion.update();
    }

    /**
     * get Line From Data Base That Equal Printed Name
     *
     * @return NameSurferEntry line from data base
     */
    private NameSurferEntry getLineFromDataBaseThatEqualPrintedName() {
        return dataBase.findEntry(printedNameTextField.getText().trim());
    }

    /**
     * draw Line belong to values that takes
     *
     * @param entry
     */
    private void drawLineFromDataBase(NameSurferEntry entry) {
        graphicRepresenrarion.addEntry(entry);
    }

    /**
     * clear Graphic Representation set text in text field to "" , call .clear()
     * from NameSurferGraph
     */
    private void clearGraphicRepresentation() {
        printedNameTextField.setText("");
        graphicRepresenrarion.clear();
    }

    /**
     * Create inner class to give Graph Button choose different actionPerformed
     * method
     */
    private class GraphButtonPress implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            drawLineFromDataBase(getLineFromDataBaseThatEqualPrintedName());
            updateGraphicRepresentation();
        }
    }

    /**
     * Create inner class to give Clean Button choose different actionPerformed
     * method
     */
    private class ClearButtonPress implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            clearGraphicRepresentation();
            updateGraphicRepresentation();
        }
    }

    /**
     * Create inner class to give Text Field abillity to react on enter button
     * pressed
     */
    private class EnterPressed implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == Event.ENTER) {
                drawLineFromDataBase(getLineFromDataBaseThatEqualPrintedName());
                updateGraphicRepresentation();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }
}
