package assignments.com.shpp.cs.lavej.Assignment7;

/*
 * File: NameSurferGraph.java
 * ---------------------------
 * This class represents the canvas on which the graph of
 * names is drawn. This class is responsible for updating
 * (redrawing) the graphs whenever the list of entries changes
 * or the window is resized.
 */
import acm.graphics.*;
import java.awt.event.*;
import java.util.*;
import java.awt.*;
import javax.swing.JLabel;

public class NameSurferGraph extends GCanvas
        implements NameSurferConstants, ComponentListener {

    /*---------------class variables-------------------*/
    /**
     * number of decades
     */
    private double[] horizontalPointInRepresentation;
    /**
     * values from data base
     */
    private double[] verticalPointInRepresentation;
    /**
     * store name of child from data base
     */
    private String nameOfChildInDataBase;
    /**
     * there will be all components for graphic representation
     */
    private ArrayList<GObject> storedGraphics = new ArrayList<>();
    /**
     * textOfNpeMessage text wil be desplayed if there is no such name in data
     * base
     */
    private String textOfNpeMessage = "";
    /**
     * stored the values from data base
     */
    private ArrayList<NameSurferEntry> internalEntry = new ArrayList<>();

    /*---------------Methods-------------------*/
    /**
     * method for drawing vertical lines
     */
    private void drawVerticalLines() {
        double x1 = getWidth() / NDECADES;
        double y1 = 0;
        double x2 = x1;
        double y2 = getHeight();

        for (int i = 0; i < NDECADES; i++) {
            GLine line = new GLine(X_OFFSET + x1 * i, y1, X_OFFSET + x2 * i, y2);
            add(line);
        }
    }

    /**
     * method for drawing horizontal lines
     */
    private void drawHorizontalLines() {
        double yOffset = getHeight() / NDECADES;
        double x1 = 0;
        double y1 = yOffset;
        double x2 = getWidth();
        double y2 = getHeight() - yOffset;
        GLine line = new GLine(x1, y1, x2, y1);
        add(line);
        GLine line2 = new GLine(x1, y2, x2, y2);
        add(line2);
    }

    /**
     * method for drawing decade names begin from 1900 end to 2010
     */
    private void drawDecadeName() {
        int decade = START_DECADE;
        double x = getWidth() / SPACING_CONSTANT;
        double fontHeight = getHeight() / SPACING_CONSTANT;
        double y = (getHeight() + (int) (DECADE_NAMES_MAGICAL_COEF * fontHeight));
        int sizeOfFont = (int) (DECADE_NAMES_MAGICAL_COEF * (x + fontHeight));

        for (int i = 0; i < NDECADES; i++) {
            GLabel label = new GLabel(decade + "");
            label.setLocation(X_OFFSET + x * i, y - label.getHeight());
            label.setFont("Serif-Bold-" + sizeOfFont);
            add(label);
            decade += 10;
        }
    }

    /**
     * if value in external file will be zero? this method turn it to
     * MAX_VALUE_IN_DATA_BASE
     */
    private void changeZeroVerticalPointValue() {
        for (int g = 0; g < NDECADES; g++) {
            if (verticalPointInRepresentation[g] == 0) {
                verticalPointInRepresentation[g] = MAX_VALUE_IN_DATA_BASE;
            }
        }
    }

    /**
     * putToArrayList lines for graphic representation of dependencies
     */
    private void putToArrayListLinesForGraphics(double horizontalPointsKoef,
            double yOffset, double verticalRange) {

        for (int i = 0; i < NDECADES - 1; i++) {
            changeZeroVerticalPointValue();
            GLine line = new GLine(horizontalPointsKoef * horizontalPointInRepresentation[i] + X_OFFSET,
                    (getHeight() * verticalPointInRepresentation[i]) / verticalRange + yOffset,
                    horizontalPointsKoef * horizontalPointInRepresentation[i + 1] + X_OFFSET,
                    (getHeight() * verticalPointInRepresentation[i + 1]) / verticalRange + yOffset);
            storedGraphics.add(line);
        }
    }

    /**
     * putToArrayList names of children from data base draw it near points of
     * lines
     */
    private void putToArrayListChildrenNameFromDataBase(double horizontalPointsKoef,
            double yOffset, double verticalRange) {
        for (int i = 0; i < NDECADES; i++) {
            GLabel name;
            if (verticalPointInRepresentation[i] == 0) {
                name = new GLabel(this.nameOfChildInDataBase + "*");
                name.setLocation(horizontalPointsKoef * horizontalPointInRepresentation[i] + X_OFFSET,
                        (getHeight() * MAX_VALUE_IN_DATA_BASE) / verticalRange + yOffset);
            } else {
                name = new GLabel(this.nameOfChildInDataBase + (int) this.verticalPointInRepresentation[i]);
                name.setLocation(horizontalPointsKoef * horizontalPointInRepresentation[i] + X_OFFSET,
                        (getHeight() * verticalPointInRepresentation[i]) / verticalRange + yOffset);
            }
            name.sendForward();
            name.setFont("Serif-Bold-15");
            storedGraphics.add(name);
        }
    }

    /**
     * redraw graphical representation, call methods wich put values to
     * arraylist storedgraphics, then calls method draw stored graphics. this
     * method will works only if there was call addEntry method, it call method
     * of putToArrayListChildrenNameFromDataBase();
     * putToArrayListLinesForGraphics(); then get values from arrayList and add
     * them to screen
     */
    private void updateStoredGraphicsList() {
        double horizontalPointsKoef = getWidth() / SPACING_CONSTANT;
        double yOffset = getHeight() / SPACING_CONSTANT;
        double verticalRange = getHeight() / NAMES_LINES_MAGICAL_COEF + MAX_VALUE_IN_DATA_BASE;
        putToArrayListChildrenNameFromDataBase(horizontalPointsKoef, yOffset, verticalRange);
        putToArrayListLinesForGraphics(horizontalPointsKoef, yOffset, verticalRange);
    }

    /**
     * set color of graphic representation
     */
    private void setColor() {
        int steps = 0;
        for (int i = 0; i < storedGraphics.size(); i++) {
            if (steps < NUMBER_OF_ELEMENTS_IN_ONE_GRAPHIC) {
                storedGraphics.get(i).setColor(Color.green);
            }
            if (steps >= NUMBER_OF_ELEMENTS_IN_ONE_GRAPHIC && steps < NUMBER_OF_ELEMENTS_IN_ONE_GRAPHIC * 2) {
                storedGraphics.get(i).setColor(Color.red);
            }
            if (steps >= NUMBER_OF_ELEMENTS_IN_ONE_GRAPHIC * 2 && steps < NUMBER_OF_ELEMENTS_IN_ONE_GRAPHIC * 3) {
                storedGraphics.get(i).setColor(Color.magenta);
            }
            if (steps >= NUMBER_OF_ELEMENTS_IN_ONE_GRAPHIC * 3 && steps < NUMBER_OF_ELEMENTS_IN_ONE_GRAPHIC * 4) {
                storedGraphics.get(i).setColor(Color.blue);
            }
            if (steps >= NUMBER_OF_ELEMENTS_IN_ONE_GRAPHIC * 4) {
                steps = 0;
                storedGraphics.get(i).setColor(Color.green);
            }
            steps++;
        }
    }

    /**
     * draw graphics that was stored in storedGraphics
     */
    private void addStoredGraphicsToScreen() {
        setColor();
        for (GObject obj : storedGraphics) {
            add(obj);
        }
    }

    /**
     * create new on screen NpeMessage
     *
     * @param text of message
     */
    private void createNpeMessage(String text) {
        JLabel npeMessage = new JLabel(text);
        npeMessage.setForeground(Color.red);
        add(npeMessage);
    }

    /**
     * Creates a new NameSurferGraph object that displays the data.
     */
    public NameSurferGraph() {
        addComponentListener(this);
        // You fill in the rest //
    }

    /**
     * Clears the list of name surfer entries stored inside this class.
     */
    public void clear() {
        internalEntry = new ArrayList<>();
        storedGraphics = new ArrayList<>();
        textOfNpeMessage = "";
    }

    /* Method: addEntry(entry) */
    /**
     * Adds a new NameSurferEntry to the list of entries on the display. Note
     * that this method does not actually draw the graph, but simply stores the
     * entry; the graph is drawn by calling update.
     */
    public void addEntry(NameSurferEntry entry) throws NullPointerException {

        if (entry != null) {
            internalEntry.add(entry);
            textOfNpeMessage = "";
        } else {
            textOfNpeMessage = NPE_MESSAGE;
        }
    }

    /**
     * methods of drawing graphics
     */
    private void drawGraphics() {
        storedGraphics = new ArrayList<>();

        for (NameSurferEntry entry : internalEntry) {
            this.nameOfChildInDataBase = entry.getName();
            horizontalPointInRepresentation = new double[NDECADES];
            verticalPointInRepresentation = new double[NDECADES];

            for (int i = 0; i < NDECADES; i++) {
                this.horizontalPointInRepresentation[i] = i;
                this.verticalPointInRepresentation[i] = entry.getRank(i);
            }
            updateStoredGraphicsList();
            addStoredGraphicsToScreen();
        }
    }

    /**
     * Updates the display image by deleting all the graphical objects from the
     * canvas and then reassembling the display according to the list of
     * entries. Your application must call update after calling either clear or
     * addEntry; update is also called whenever the size of the canvas changes.
     */
    public void update() {
        removeAll();
        drawVerticalLines();
        drawHorizontalLines();
        drawDecadeName();
        if (textOfNpeMessage.equals(NPE_MESSAGE)) {
            createNpeMessage(textOfNpeMessage);
        }
        drawGraphics();
    }

    /* Implementation of the ComponentListener interface */
    public void componentHidden(ComponentEvent e) {
    }

    public void componentMoved(ComponentEvent e) {
    }

    public void componentResized(ComponentEvent e) {
        update();
    }

    public void componentShown(ComponentEvent e) {
    }
}
