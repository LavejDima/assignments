package assignments.com.shpp.cs.lavej.Assignment7;

/*
 * File: NameSurferEntry.java
 * --------------------------
 * This class represents a single entry in the database.  Each
 * NameSurferEntry contains a name and a list giving the popularity
 * of that name for each decade stretching back to 1900.
 */
import java.util.*;

public class NameSurferEntry implements NameSurferConstants {

    private String myLine = "";

    /* Constructor: NameSurferEntry(line) */
    /**
     * Creates a new NameSurferEntry from a data line as it appears in the data
     * file. Each line begins with the name, which is followed by integers
     * giving the rank of that name for each decade.
     */
    public NameSurferEntry(String line) {
        this.myLine = line;
    }

    /* Method: getName() */
    /**
     * Returns the name associated with this entry.
     */
    public String getName() {
        return getNameFromLine(myLine);
    }

    /* Method: getRank(decade) */
    /**
     * Returns the rank associated with an entry for a particular decade. The
     * decade value is an integer indicating how many decades have passed since
     * the first year in the database, which is given by the constant
     * START_DECADE. If a name does not appear in a decade, the rank value is 0.
     */
    public int getRank(int decade) {
        return getIntsFromLine(myLine, decade);
    }

    /* Method: toString() */
    /**
     * Returns a string that makes it easy to see the value of a
     * NameSurferEntry.
     */
    public String toString() {
        String[] content = getAllContentFromLine(myLine);
        String s = "";
        for (int i = 0; i < content.length; i++) {
            if (i == 0) {
                s += content[i] + " [";
            } else if (i == content.length - 1) {
                s += content[i] + "]";
            } else {
                s += content[i] + " ";
            }
        }
        return s;
    }

    /**
     * take strin from data base and parse it depend on (spacing) " " then store
     * it to array and return
     *
     * @param line from data base
     * @return String[] of values and name
     */
    private String[] getAllContentFromLine(String line) {
        return line.split(" ");
    }

    /**
     * take String of line, and get name from it
     *
     * @param line
     * @return String name
     */
    private String getNameFromLine(String line) {
        return getAllContentFromLine(line)[0];
    }

    /**
     * take String line and return value depend on decade number
     *
     * @param String line from data base
     * @param numberOfDecade to find the value depend on decade
     * @return nomber which is value
     */
    private int getIntsFromLine(String line, int numberOfDecade) {
        String[] content = getAllContentFromLine(line);
        int[] numbers = new int[content.length];
        for (int i = 0; i < numbers.length - 1; i++) {
            numbers[i] = Integer.parseInt(content[i + 1]);
        }
        return numbers[numberOfDecade];
    }
}
