package assignments.com.shpp.cs.lavej.Assignment7;

/*
 * File: NameSurferConstants.java
 * ------------------------------
 * This file declares several constants that are shared by the
 * different modules in the NameSurfer application.  Any class
 * that implements this interface can use these constants.
 */
public interface NameSurferConstants {

    /**
     * The width of the application window
     */
    public static final int APPLICATION_WIDTH = 800;
    /**
     * The height of the application window
     */
    public static final int APPLICATION_HEIGHT = 600;
    /**
     * The name of the file containing the data
     */
    public static final String NAMES_DATA_FILE = "/home/lavej/NetBeansProjects/namesurfer"
            + "/src/com/shpp/cs/namesurfer/Assignment7/names-data.txt";
    /**
     * The first decade in the database
     */
    public static final int START_DECADE = 1900;
    /**
     * The number of decades
     */
    public static final int NDECADES = 12;
    /**
     * The maximum rank in the database
     */
    public static final int MAX_RANK = 1000;
    /**
     * The number of pixels to reserve at the top and bottom
     */
    public static final int GRAPH_MARGIN_SIZE = 20;
    /* my constants that simples drawing lines , names and decades in true way*/
    /**
     * offset of vertical lines, lines of graphical representation, names of
     * childrens and names of decade
     */
    public static final double X_OFFSET = 10;
    /**
     * helps to resize decade names
     */
    public static final double DECADE_NAMES_MAGICAL_COEF = 0.17;
    /**
     * helps to vertical positioning of graphical represent
     */
    public static final double NAMES_LINES_MAGICAL_COEF = 2.6;
    /**
     * maximal value that may be stored in data base
     */
    public static final double MAX_VALUE_IN_DATA_BASE = 1000;
    /**
     * spasing constant
     */
    public static final int SPACING_CONSTANT = 12;
    /**
     * text of npe message, appears when there is no such name in data base
     */
    public static final String NPE_MESSAGE = "there is no such name in base, enter something else";
    /**
     * number of lines and name which is stored in array list and create one
     * graphic
     */
    public static final int NUMBER_OF_ELEMENTS_IN_ONE_GRAPHIC = 23;
}
