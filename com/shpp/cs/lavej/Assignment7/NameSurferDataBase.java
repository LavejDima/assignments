package assignments.com.shpp.cs.lavej.Assignment7;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * File: NameSurferDataBase.java
 * -----------------------------
 * This class keeps track of the complete database of names.
 * The constructor reads in the database from a file, and
 * the only public method makes it possible to look up a
 * name and get back the corresponding NameSurferEntry.
 * Names are matched independent of case, so that "Eric"
 * and "ERIC" are the same names.
 */
public class NameSurferDataBase implements NameSurferConstants {

    /**
     * lines stores in arrayList
     */
    private ArrayList<String> content = new ArrayList<>();

    /* Constructor: NameSurferDataBase(filename) */
    /**
     * Creates a new NameSurferDataBase and initializes it using the data in the
     * specified file. The constructor throws an error exception if the
     * requested file does not exist or if an error occurs as the file is being
     * read.
     */
    public NameSurferDataBase(String filename) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            while (true) {
                String s = reader.readLine();
                if (s == null) {
                    break;
                }
                content.add(s);
                NameSurferEntry entry = new NameSurferEntry(s);
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException");
        } catch (IOException ex) {
            System.out.println("IOException");
        }
    }

    /* Method: findEntry(name) */
    /**
     * Returns the NameSurferEntry associated with this name, if one exists. If
     * the name does not appear in the database, this method returns null.
     */
    public NameSurferEntry findEntry(String name) {
        String nameFromBase = "";
        for (int i = 0; i < content.size(); i++) {
            nameFromBase = new NameSurferEntry(content.get(i)).getName();
            if (nameFromBase.equalsIgnoreCase(name)) {
                return new NameSurferEntry(content.get(i));
            }
        }
        return null;
    }
}
