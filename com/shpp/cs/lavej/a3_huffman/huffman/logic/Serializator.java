package assignments.com.shpp.cs.lavej.a3_huffman.huffman.logic;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Serialize CodedObject
 */
public class Serializator {

    /**
     * De-serialize CodedObject
     *
     * @param absolutePath of file to de-serialize
     * @return CodedObject
     */
    public CodedObject deserialize(String absolutePath) {
        CodedObject codedObject = null;
        try {
            System.out.println("De-serialize " + absolutePath);
            FileInputStream fileIn = new FileInputStream(absolutePath);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            codedObject = (CodedObject) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("file wasnt correctly coded");
        }
        return codedObject;
    }

    /**
     * Serialize CodedObject
     *
     * @param fileToSave file that must be serialized
     * @param fileLocation where save this file
     * @return location of serialized file
     */
    public String serialize(CodedObject fileToSave, String fileLocation) {
        try {
            FileOutputStream fileOut = new FileOutputStream(fileLocation);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(fileToSave);
            out.close();
            fileOut.close();
            System.out.println("Serialized data is saved in " + fileLocation);
        } catch (FileNotFoundException e) {
            System.err.println("file " + fileLocation + " wasnt found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileLocation;
    }
}
