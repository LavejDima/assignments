package assignments.com.shpp.cs.lavej.a3_huffman.huffman.logic;

import java.util.BitSet;
import java.util.Comparator;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.PriorityQueue;

public class HuffmanLogic {

    //Hufman table
    private HashMap<Byte, String> table = new HashMap<>();
    //must begins from 1 to code 1 byte file
    private String codes = "1";
    //just for cast bytes to array indexes
    private static final int COEF = 128;

    /**
     * get bytes of file
     *
     * @param fileName
     * @return array of bytes which is file representation
     */
    public byte[] getBytesFromFile(String fileName) {
        try {
            File file = new File(fileName);
            InputStream is = new FileInputStream(file);
            long length = file.length();
            //understand how big is file
            if (length > Integer.MAX_VALUE) {
                return null;
            }
            byte[] b = new byte[(int) length];
            // Read in the bytes
            int offset = 0;
            int numRead = 0;
            while (offset < b.length && (numRead = is.read(b, offset, b.length - offset)) >= 0) {
                offset += numRead;
            }
            // Ensure all the bytes have been read in
            if (offset < b.length) {
                System.err.println("Could not completely read file " + file.getName());
                return null;
            }
            // Close the input stream and return bytes
            is.close();
            return b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * get freq. of byte appearing in file
     *
     * @param bytes array file representation
     * @return frequencies of byte appearing in file
     */
    private int[] getByteFrequencies(byte[] bytes) {
        int[] freq = new int[256];
        for (byte currentByte : bytes) {
            int index = currentByte + COEF;
            if (freq != null) {
                freq[index] += 1;
            } else {
                freq[index] = 1;
            }
        }
        return freq;
    }

    /**
     * get nodes ranged in order
     *
     * @param frequencies hash map of byte apear freq.
     * @return priority queue of nodes
     */
    private PriorityQueue<Node> createNodes(int[] freq) {
        PriorityQueue<Node> rangedNodes = new PriorityQueue<>(new NodeComparator());
        for (int i = 0; i < freq.length; i++) {
            if (freq[i] != 0) {
                Node node = new Node((byte) (i - COEF), freq[i]);
                rangedNodes.offer(node);
            }
        }
        return rangedNodes;
    }

    /**
     * get Huffman tree from ranged nodes
     *
     * @param tree priority queue of nodes
     * @return Huffman tree
     */
    private Node getHuffmanTree(PriorityQueue<Node> tree) {
        while (tree.size() > 1) {
            Node a = tree.poll();
            Node b = tree.poll();
            tree.offer(new Node(a, b));
        }
        return tree.poll();
    }

    /**
     * get Huffman table
     *
     * @param root Huffman tree root
     */
    private void builtTable(Node root) {

        if (root.left != null) {
            codes += 0;
            builtTable(root.left);
        }
        if (root.right != null) {
            codes += 1;
            builtTable(root.right);
        }
        if (root.right == null || root.left == null) {
//            char k = (char) root.key;
//            System.out.println("key " + k + " code " + codes);
            table.put(root.key, codes);
            root.code = codes;
            codes = codes.substring(0, codes.length() - 1);
        } else if (codes.length() > 0) {
            codes = codes.substring(0, codes.length() - 1);
        }

    }

    /**
     * decode coded file
     *
     * @param table Huffman table
     * @param bitSet coded file
     * @return array of byte which is equals array of byte of file which was
     * coded
     */
    private byte[] decode(HashMap<Byte, String> table, BitSet bitSet) {
        int pos = 0;
        byte[] byteArray = new byte[bitSet.length()];
        String temp = "";
        HashMap<String, Byte> tableReversed = new HashMap<>();
        for (byte b : table.keySet()) {
            tableReversed.put(table.get(b), b);
        }
        for (int i = 0; i < byteArray.length; i++) {
            temp += bitSet.get(i) ? 1 : 0;
            if (tableReversed.containsKey(temp)) {
                byte by = tableReversed.get(temp);
                byteArray[pos++] = by;
                temp = "";
            }
        }
        return Arrays.copyOfRange(byteArray, 0, pos);
    }

    /**
     * code the file
     *
     * @param table Huffman table
     * @param bytesToCode file byte representation which must be coded
     * @return BitSet of coded file
     */
    private BitSet code(HashMap<Byte, String> table, byte[] bytesToCode) {
        int position = 0;
        BitSet bitSet = new BitSet();
        for (byte oneByte : bytesToCode) {
            if (table.containsKey(oneByte)) {
                String currentCode = table.get(oneByte);
                for (int i = 0; i < currentCode.length(); i++) {
                    char currentChar = currentCode.charAt(i);
                    if (currentChar == '1') {
                        bitSet.set(position);
                        position++;
                    } else if (currentChar == '0') {
                        position++;
                    }
                }
            }
        }
        if (table.size() > 2) {
            bitSet.set(position++);
            return bitSet;
        }
        return bitSet;
    }

    /**
     * create CodedObject which serialize later
     *
     * @param fileName name of file
     * @return CodedObject
     */
    public CodedObject huffmanCodeFile(String fileName) {
        byte[] bytesFromFile = getBytesFromFile(fileName);
        Node tree = getHuffmanTree(createNodes(getByteFrequencies(bytesFromFile)));
        builtTable(tree);
        return new CodedObject(code(table, getBytesFromFile(fileName)), table);
    }

    /**
     * decode coded file.
     *
     * @param table Huffman table
     * @param bitSet coded file
     * @return array of byte which is equals array of byte of file which was
     * coded
     */
    public byte[] huffmanDecodeFile(HashMap<Byte, String> table, BitSet bitSet) {
        return decode(table, bitSet);
    }

    /**
     * Node comparator class(for correct sorting)
     */
    private class NodeComparator implements Comparator<Node> {

        @Override
        public int compare(Node o1, Node o2) {
            return o1.frequency > o2.frequency ? 1 : o1.frequency == o2.frequency ? 0 : -1;
        }

    }

    /**
     * Node class
     */
    private class Node implements Comparable<Node> {

        //symbol
        private byte key;

        //leter frequency
        private int frequency;

        private String code;

        //childrens
        private Node left;
        private Node right;

        /**
         * initialization of parameters in constructor
         *
         * @param symbol
         * @param frequency
         */
        Node(byte symbol, int frequency) {
            this.key = symbol;
            this.frequency = frequency;
        }

        /**
         * initialization children's in constructor
         *
         * @param left
         * @param right
         */
        Node(Node left, Node right) {
            this.left = left;
            this.right = right;
            this.frequency += left.frequency + right.frequency;
        }

        /**
         * empty constructor
         */
        Node() {
        }

        /**
         * need to implement comparable for sorting correctlly
         *
         * @param node
         * @return
         */
        @Override
        public int compareTo(Node node) {
            return node.frequency > this.frequency ? -1 : node.frequency == this.frequency ? 0 : 1;
        }

        /**
         * just for better understanding what happens
         *
         * @return
         */
        @Override
        public String toString() {
            return this.key + ":" + this.frequency + " code -> " + code;
        }
    }
}
