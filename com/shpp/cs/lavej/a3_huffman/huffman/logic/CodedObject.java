package assignments.com.shpp.cs.lavej.a3_huffman.huffman.logic;

import java.io.Serializable;
import java.util.BitSet;
import java.util.HashMap;

/**
 * Object to save
 */
public class CodedObject implements Serializable {

    //coded bits
    private BitSet bitset;
    //table to decode
    private HashMap<Byte, String> table;

    //init the object
    public CodedObject(BitSet bitset, HashMap<Byte, String> table) {
        this.bitset = bitset;
        this.table = table;
    }

    /**
     * get the coded object bytes
     *
     * @return the coded object
     */
    public BitSet getBitSet() {
        return this.bitset;
    }

    /**
     * get the table to decode object
     *
     * @return table to decode object
     */
    public HashMap<Byte, String> getTable() {
        return this.table;
    }

}
