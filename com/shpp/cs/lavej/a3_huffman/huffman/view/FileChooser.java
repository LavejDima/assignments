package assignments.com.shpp.cs.lavej.a3_huffman.huffman.view;

import assignments.com.shpp.cs.lavej.a3_huffman.huffman.logic.CodedObject;
import assignments.com.shpp.cs.lavej.a3_huffman.huffman.logic.HuffmanLogic;
import assignments.com.shpp.cs.lavej.a3_huffman.huffman.logic.Serializator;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Font;
import java.io.FileOutputStream;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class FileChooser {

    private JFrame frame;
    private JButton open;
    private JButton code;
    private JButton decode;
    private JPanel panel;
    private JFileChooser fileChooser;
    private JTextArea textArea;
    private File fileToManipulate;
    private CodedObject fileToSave;

    public static void main(String[] args) {
        new FileChooser().run();
    }

    private void run() {
        //----------------File Chooser----------------
        fileChooser = new JFileChooser();

        //----------------TextArea--------------------
        textArea = new JTextArea("Nothing is selected");
        textArea.setEditable(false);
        textArea.setFont(new Font("Arial", 0, 18));

        //-----------------ScrollBar------------------
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setBounds(10, 60, 780, 500);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        //-----------------Buttons--------------------
        open = new JButton("open");
        code = new JButton("code");
        decode = new JButton("decode");
        //ActionListeners for buttons
        open.addActionListener(new OpenButtonListener());
        code.addActionListener(new CodeButtonListener());
        decode.addActionListener(new DecodeButtonListener());

        //---------------Panel------------------------
        panel = new JPanel();
        panel.add(open);
        panel.add(code);
        panel.add(decode);

        //--------------Frame-------------------------
        frame = new JFrame("File Chooser");
        frame.getContentPane().add(panel, BorderLayout.SOUTH);
        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setVisible(true);

    }

    private class OpenButtonListener implements ActionListener {
//Open the file

        @Override
        public void actionPerformed(ActionEvent e) {
            openFile();
        }

        private void openFile() {
            int fileChooserCode = fileChooser.showOpenDialog(frame);
            if (fileChooserCode == JFileChooser.CANCEL_OPTION) {
                frame.remove(fileChooser);
            }
            if (fileChooserCode == JFileChooser.APPROVE_OPTION) {
                fileToManipulate = fileChooser.getSelectedFile();
                textArea.setText("File (" + fileToManipulate + ") is selected");
            }
        }

    }

    private class CodeButtonListener implements ActionListener {
//Code opened file

        @Override
        public void actionPerformed(ActionEvent e) {
            zip();
        }

        private void zip() {
            //if there is no such file
            if (fileToManipulate == null) {
                zipNullFile();
            } else if (fileToManipulate.length() == 0) {
                zipEmptyFile();
            } else {
                zipInternal();
            }
        }

        private void zipNullFile() {
            textArea.setText("Nothing is selected" + "\n" + "Please choose the file");
        }

        private void zipEmptyFile() {
            textArea.setText("File " + fileToManipulate.getAbsolutePath() + " is empty" + "\n" + "Please choose the file");
            resetFileToManipulate();
        }

        private void resetFileToManipulate() {
            fileToManipulate = null;
        }

        private void zipInternal() {
            fileToSave = new HuffmanLogic().huffmanCodeFile(fileToManipulate.getAbsolutePath());
            textArea.setText(textArea.getText() + "\n" + "File was coded" + "\n" + "Save the file");
            int fileChooserCode = fileChooser.showSaveDialog(frame);
            if (fileChooserCode == JFileChooser.CANCEL_OPTION) {
                frame.remove(fileChooser);
                textArea.setText(textArea.getText() + "\n" + "File wasnt saved");
            }
            if (fileChooserCode == JFileChooser.APPROVE_OPTION) {
                textArea.setText(textArea.getText() + "\n" + "File(" + fileChooser.getSelectedFile() + ") was saved");
                new Serializator().serialize(fileToSave, fileChooser.getSelectedFile().getAbsolutePath());
            }
            //reset the class variables
            fileToSave = null;
            resetFileToManipulate();
        }
    }

    private class DecodeButtonListener implements ActionListener {
//Decode opened file

        @Override
        public void actionPerformed(ActionEvent e) {
            unzip();
        }

        private void unzip() {
//if there is no opened file
            if (fileToManipulate == null) {
                unzipNullFile();
            } else if (fileToManipulate.length() == 0) {
                unzipEmptyFile();
            } else {
                unzipInternal();
            }
        }

        private void unzipNullFile() {
            textArea.setText("Nothing is selected" + "\n" + "Please choose the file");
        }

        private void unzipEmptyFile() {
            textArea.setText("File " + fileToManipulate.getAbsolutePath() + " is empty" + "\n" + "Please choose the file");
            resetFileToManipulate();
            fileToSave = null;
        }

        private void unzipInternal() {
            CodedObject desObj = new Serializator().deserialize(fileToManipulate.getAbsolutePath());
            if (desObj == null) {
                unzipUncorrectCodedFile();
            } else {
                unzipCorrectCodedFile(desObj);
            }
            //reset the file link
            resetFileToManipulate();

        }

        private void unzipUncorrectCodedFile() {
            textArea.setText("File " + fileToManipulate.getAbsolutePath() + " wasnt coded" + "\n" + "Please choose the file");
        }

        private void unzipCorrectCodedFile(CodedObject desObj) {
            byte[] decoded = new HuffmanLogic().huffmanDecodeFile(desObj.getTable(), desObj.getBitSet());
            System.out.println("was decoded");
            textArea.setText(textArea.getText() + "\n" + decoded + "\n" + "File was decoded");
            int fileChooserCode = fileChooser.showSaveDialog(frame);
            if (fileChooserCode == JFileChooser.CANCEL_OPTION) {
                frame.remove(fileChooser);
                textArea.setText(textArea.getText() + "\n" + "File wasnt saved");
            }
            if (fileChooserCode == JFileChooser.APPROVE_OPTION) {
                try {
                    FileOutputStream fos = new FileOutputStream(fileChooser.getSelectedFile().getAbsolutePath());
                    fos.write(decoded);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                textArea.setText(textArea.getText() + "\n" + "File(" + fileChooser.getSelectedFile() + ") was saved");
            }

        }

        private void resetFileToManipulate() {
            fileToManipulate = null;
        }

    }

}
