package assignments.com.shpp.cs.lavej.a1_calculator;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;

public class Calculator {
//----------------------------------fields of class------------------------------------

    private static final String OPERATORS = "-+/*^()";
    private static final String SIGNS = "d" + OPERATORS + ".";
    private static final String FUNCTIONS_TO_REPLACE = "sin\\(+|cos\\(+|sqrt\\(+|tan\\(+";
    private HashMap<Character, Double> userDefVars;
    private HashMap<Character, Integer> operatorsPriority = new HashMap<>();
    private HashMap<String, IFunc> functions = new HashMap<>();
//---------------------------------initialize fields of class--------------------------

    {
        operatorsPriority.put('^', 3);
        operatorsPriority.put('*', 2);
        operatorsPriority.put('/', 2);
        operatorsPriority.put('+', 1);
        operatorsPriority.put('-', 1);
    }

    {
        functions.put("sin", new IFunc() {
            @Override
            public String calculateFunct(String d) {
                return BigDecimal.valueOf(Math.sin(Math.toRadians(Double.parseDouble(d)))).toPlainString();
            }
        });
        functions.put("cos", new IFunc() {
            @Override
            public String calculateFunct(String d) {
                return BigDecimal.valueOf(Math.cos(Math.toRadians(Double.parseDouble(d)))).toPlainString();
            }
        });
        functions.put("tan", new IFunc() {
            @Override
            public String calculateFunct(String d) {
                return BigDecimal.valueOf(Math.tan(Math.toRadians(Double.parseDouble(d)))).toPlainString();
            }
        });
        functions.put("sqrt", new IFunc() {
            @Override
            public String calculateFunct(String d) {
                double number = Double.parseDouble(d);
                if (number < 0) {
                    System.err.println("incorrect data for sqrt");
                    return "";
                }
                return BigDecimal.valueOf(Math.sqrt(number)).toPlainString();
            }
        });
    }

//---------------------------------constructors----------------------------------
    public Calculator() {
    }

    public Calculator(HashMap<Character, Double> variables) {
        this.userDefVars = variables;
    }

//----------------------------main logic methods----------------------------------
    //calculate expression in rpn presentation
    private double calculateExpression(String expression, boolean isNotAFunction) {
        expression = validate(expression, isNotAFunction);
        String[] tokens = expression.split(" ");
        Stack<Double> stack = new Stack<>();
        for (String token : tokens) {
            // put numbers to stack
            if (Character.isDigit(token.isEmpty() ? ' ' : token.charAt(0))) {
                stack.push(Double.parseDouble(token));
            } //solve small parts of expression
            else {
                double operand2 = stack.isEmpty() ? 0.0 : stack.pop();
                //works when you write 10-
                if (stack.isEmpty()) {
                    break;
                }
                double operand1 = stack.pop();

                if (token.equals("^")) {
                    Double d = Math.pow(operand1, operand2);
                    if (d.isNaN() || d.isInfinite()) {
                        System.err.println("Math.pow returns " + d);
                        return 0;
                    }
                    stack.push(d);
                } else if (token.equals("*")) {
                    stack.push(operand1 * operand2);
                } else if (token.equals("/")) {
                    if (operand2 == 0) {
                        System.err.println("division by zero");
                        return 0;
                    }
                    stack.push(operand1 / operand2);
                } else if (token.equals("+")) {
                    stack.push(operand1 + operand2);
                } else if (token.equals("-")) {
                    stack.push(operand1 - operand2);
                }
            }
        }
        if (stack.size() != 1) {
//            throw new IllegalArgumentException("Expression syntax error.");
            System.err.println("Expression syntax error.");
            return 0;
        }
        return stack.pop();
    }

    public double calculateExpression(String expression) {
        return calculateExpression(expression, true);
    }

    //convert string formula to reverse poland notation representation
    private String toRPN(String input) {
        int inputStringLength = input.length();
        Stack<Character> stack = new Stack<>();
        String output = "";
        for (int i = 0; i < inputStringLength; i++) {
            //currentChar
            char currentChar = input.charAt(i);
            //is current symbol number or is current symbol part of number
            if (isNumber(currentChar)) {
                //check if this symbol is last, or operator, or parenthes and put space after him
                if (i + 1 == inputStringLength
                        || operatorsPriority.containsKey(input.charAt(i + 1))
                        || input.charAt(i + 1) == '('
                        || input.charAt(i + 1) == ')') {
                    output += currentChar + " ";
                }//append current symbol to output string 
                else {
                    output += currentChar;
                }
            }//if symbol is open parenthes than push it to stack
            else if (isOpenParenthes(currentChar)) {
                stack.push('(');
            } //if symbol is close parenthes pop from stack until find open parenthes
            else if (isCloseParenthes(currentChar)) {
                while (stack.peek() != '(') {
                    output += stack.pop() + " ";
                }
                //delete open parenthes from stack
                stack.pop();
            } else if (operatorsPriority.containsKey(currentChar)) {
                while (!(stack.isEmpty()
                        || operatorsPriority.get(stack.peek()) == null) && (operatorsPriority.get(stack.peek()) >= operatorsPriority.get(currentChar))) {
                    output += stack.pop() + " ";
                }
                stack.push(currentChar);
            } // error 
            else {
//                throw new IllegalArgumentException("Input String Error");
                System.err.println("Input String Error");
                return null;
            }
        }
        // get all symbols from stack
        if (stack.size() > 0) {
            for (int i = 0; i < stack.size() - 1; i++) {
                output += stack.pop() + " ";
            }
            output += stack.pop();
        }
//        System.out.print(" rpn = " + output + " ");
        return output;
    }

    private String validate(String expression, boolean isNotAFunction) {
        if (expression != null) {
            if (isNotAFunction) {
                //delete whitespaces
                expression = deleteWhiteSpaces(expression);
                if (!isBracketsCorrect(expression)) {
                    System.err.println("brackets is incorrect");
                    return "0";
                }
                if (!isFormulaContainRightSymbols(expression, userDefVars)) {
                    System.err.println("incorrect symbols are founded");
                    return "0";
                }
                if (!isNumberOfDotsCorrect(expression)) {
                    System.err.println("number of dots is incorrect");
                    return "0";
                }
                //get values instead userDefVars
                expression = replaceVariableByValues(expression);
                if (!isFunctionsNamedCorrect(expression)) {
                    System.err.println("name of function is incorrect");
                    return "0";
                }
                //replace functions by solved values
                expression = functionGlobalCutter(expression);
            }
            //change simple minus before number into (0-number)
            expression = modifyUnaryMinus(expression);
            if (!isDoubleSignsCorrect(expression)) {
                System.err.println("double signs");
                return "0";
            }
            //convert expression view into rpn view
            expression = toRPN(expression);
        } else {
            expression = "0";
        }
        return expression;
    }

//-----------------------------work with minus before numbers----------------------------
    //get ability to use unary minus symbol before numbers
    private String modifyUnaryMinus(String formula) {
        String result = formula;
        String beforeMinus = "";
        String afterMinus = "";
        for (int i = 0; i < result.length(); i++) {
            char currentChar = result.charAt(i);
            //if first char is '-'
            result = (i == 0 && currentChar == '-') ? 0 + result : result;
            //if minus else were in formula
            if ((i > 0) && currentChar == '-'
                    && (operatorsPriority.containsKey(result.charAt(i - 1))
                    || isOpenParenthes(result.charAt(i - 1)))) {
                beforeMinus = result.substring(0, i);
                afterMinus = result.substring(i + 1);
                for (int j = 0; j < afterMinus.length(); j++) {
                    boolean isCurrCharPartOfNumber = isNumber(afterMinus.charAt(j));
                    boolean isLastSymbolNumber = j == afterMinus.length() - 1 && isNumber(afterMinus.charAt(afterMinus.length() - 1));
                    afterMinus = !isCurrCharPartOfNumber ? "(" + "0-" + afterMinus.substring(0, j) + ")" + afterMinus.substring(j) : afterMinus;
                    afterMinus = isLastSymbolNumber ? "(" + "0-" + afterMinus + ")" : afterMinus;
                    if (!isCurrCharPartOfNumber || isLastSymbolNumber) {
                        result = beforeMinus + afterMinus;
                        i++;
                        break;
                    }
                }
            }
        }
        return result;
    }

//-----------------------------work with userDefVars---------------------------------------
    //get ability to use one-char userDefVars
    private String replaceVariableByValues(String formula) {
        if (userDefVars == null || userDefVars.isEmpty()) {
            return formula;
        }

        for (int i = 0; i < formula.length(); i++) {
            char currentChar = formula.charAt(i);
            boolean isVarExist = userDefVars.containsKey(currentChar);
            boolean isCurrCharLetter = Character.isLetter(currentChar);
            boolean isNextCharLetter = i < formula.length() - 1 ? Character.isLetter(formula.charAt(i + 1)) : false;
            boolean isPrevCharLetter = i > 0 ? Character.isLetter(formula.charAt(i - 1)) : false;
            boolean isItMiddleOfStr = i > 0 && i < formula.length() - 1;
            boolean isItEndOfStr = i == formula.length() - 1;
            boolean isItFirstPos = i == 0 && formula.length() > 1;
            boolean isItOneSymbolStr = formula.length() == 1;
            boolean isVarInMiddle = isItMiddleOfStr
                    && (isCurrCharLetter && !isNextCharLetter && !isPrevCharLetter)
                    && isVarExist;
            boolean isVarInEnd = isItEndOfStr
                    && (isCurrCharLetter && formula.length() > 1 && !isPrevCharLetter)
                    && isVarExist;
            boolean isVarInFirstPos = isItFirstPos
                    && (isCurrCharLetter && !isNextCharLetter)
                    && isVarExist;
            boolean isStringOfOneVar = isItOneSymbolStr
                    && isCurrCharLetter
                    && isVarExist;
            if (isVarInMiddle || isVarInEnd || isVarInFirstPos || isStringOfOneVar) {
                formula = formula.substring(0, i)
                        + (userDefVars.get(currentChar) < 0 ? "(0" + userDefVars.get(currentChar) + ")" : userDefVars.get(currentChar))
                        + formula.substring(i + 1);
            }
        }
        return formula;
    }

//-----------------------------------work with functions-----------------------------------
    private String functionGlobalCutter(String formulaWithFunctions) {
        if (formulaWithFunctions == null) {
            System.err.println("incorrect function naming");
            return "";
        }
        String functionName = "";
        Stack<String> stack = new Stack<>();
        for (int i = 0; i < formulaWithFunctions.length(); i++) {
            char currentChar = formulaWithFunctions.charAt(i);
            if (Character.isLetter(currentChar)) {
                functionName += currentChar + "";
            } else if (!functionName.isEmpty()) {
                stack.push(functionName);
                functionName = "";
            }
        }
        while (!stack.isEmpty()) {
            return functionGlobalCutter(functionOrdinaryCutter(stack.pop(), formulaWithFunctions));
        }
        return formulaWithFunctions;
    }

    //works only in functionGlobalCutter (getting the right information from functionGlobalCutter to solve one function)
    private String functionOrdinaryCutter(String function, String formula) {
        //works for sin(, cos(, tan( sqrt(
        int lengthOfFunctionConstString = function.length() + 1;
        char bracket = formula.charAt(formula.lastIndexOf(function.charAt(0)) + function.length());
        if (bracket != '(') {
            System.err.println("incorrect data for function");
            return "";
        }
        int indexOfBegin = formula.lastIndexOf(function.charAt(0));
        String result = "";
        Stack<Character> stack = new Stack<>();
        //begins from brackets
        for (int i = indexOfBegin + lengthOfFunctionConstString - 1; i < formula.length(); i++) {
            char currentSymbol = formula.charAt(i);
            //just to correct find end of function, its end bracket
            if (currentSymbol == '(') {
                stack.push(currentSymbol);
            } else if (currentSymbol == ')' && !stack.isEmpty()) {
                stack.pop();
            }
            //when we find the end of function, its index is i variable
            if (stack.isEmpty()) {
                //get the whole function representation in string
                String wholeFunctionToReplace = formula.substring(indexOfBegin + lengthOfFunctionConstString, i);
                if (wholeFunctionToReplace.isEmpty() || wholeFunctionToReplace.equals(".")) {
                    System.err.println("no data for function solving");
                    return "";
                }
                if (wholeFunctionToReplace.replaceAll("[\\" + OPERATORS + "]", "").length() < wholeFunctionToReplace.length()) {
                    wholeFunctionToReplace = calculateExpression(wholeFunctionToReplace, false) + "";
                }
                result = this.functions.get(function).calculateFunct(wholeFunctionToReplace) + formula.substring(i + 1);
                return indexOfBegin != 0 ? formula.substring(0, indexOfBegin) + result : result;
            }
        }
        return null;
    }

//-----------------------------------helpers------------------------------------------------
    private boolean isNumber(char operator) {
        return ((operator >= '0' && operator <= '9') || (operator == '.'));
    }

    private boolean isOpenParenthes(char operator) {
        return (operator == '(');
    }

    private boolean isCloseParenthes(char operator) {
        return (operator == ')');
    }

    /**
     * Understand is brackets correct
     *
     * @param formula string representation of formula
     * @return true if there is brackets correctness
     */
    private boolean isBracketsCorrect(String formula) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < formula.length(); i++) {
            char currentChar = formula.charAt(i);
            //the right following of brackets
            if (currentChar == '(') {
                stack.push(currentChar);
            } else if (currentChar == ')') {
                char bracketFromStack = stack.isEmpty() ? '0' : stack.pop();
                if (currentChar == ')' && bracketFromStack != '(') {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    /**
     * delete WhiteSpaces
     *
     * @param formula string representation of formula
     * @return formula string without whiteSpaces
     */
    private String deleteWhiteSpaces(String formula) {
        return formula.replaceAll(" ", "");
    }

    /**
     * is Number Of Dots Correct
     *
     * @param formula string representation of the formula
     * @return true if number of dots is correct
     */
    private boolean isNumberOfDotsCorrect(String formula) {
        int dotCount = 0;
        for (int i = 0; i < formula.length(); i++) {
            char currentChar = formula.charAt(i);
            if (Character.isDigit(currentChar)) {
            } else if (currentChar == '.') {
                if (++dotCount > 1) {
                    return false;
                }
            } else {
                dotCount = 0;
            }
        }
        return true;
    }

    /**
     * isFormulaContainRightSymbols
     *
     * @param formula string representation of formula
     * @param userDefVars userDefVars and their values
     * @return true if there is no incorrect symbols
     */
    private boolean isFormulaContainRightSymbols(String formula, HashMap<Character, Double> userDefVars) {
        formula = formula.replaceAll("[\\" + SIGNS + "]", "");
        if (!(functions == null || functions.isEmpty())) {
            for (String function : functions.keySet()) {
                formula = formula.replaceAll(function, "");
            }
        }
        if (!(userDefVars == null || userDefVars.isEmpty())) {
            for (char var : userDefVars.keySet()) {
                formula = formula.replaceAll(var + "", "");
            }
        }
        if (formula.length() > 0) {
            System.err.println("symbol " + formula);
            return false;
        }
        return true;
    }

    /**
     * double signs checker
     *
     * @param formula string representation of formula
     * @return true if there is more than two signs following one by one
     */
    private boolean isDoubleSignsCorrect(String formula) {
        for (int i = 0; i < formula.length() - 1; i++) {
            char currentChar = formula.charAt(i);
            if (operatorsPriority.containsKey(currentChar) && operatorsPriority.containsKey(formula.charAt(i + 1))) {
                System.err.println("at pos (" + i + ") sign " + formula.substring(i));
                return false;
            }
        }
        return true;
    }

    /**
     * check the correctness of function naming
     *
     * @param formula
     * @return false if name of function is not correct
     */
    private boolean isFunctionsNamedCorrect(String formula) {
        String stringWithoutFunctions = formula.replaceAll(FUNCTIONS_TO_REPLACE, "");
        return stringWithoutFunctions.length() == stringWithoutFunctions.replaceAll("[a-z]", "").length();
    }
//-------------------------------------get formula from command line----------------------------------

    private String getFormula() {
        try {
            System.out.print("enter formula: ");
            return new BufferedReader(new InputStreamReader(System.in)).readLine();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private HashMap<Character, Double> getVarsFromCmdLine() {
        HashMap<Character, Double> variables = null;
        try {
            System.out.print("enter variables(like a:2,b:-3...and so on(when end press enter)): ");
            String variablesFromConsole = new BufferedReader(new InputStreamReader(System.in)).readLine();
            if (variablesFromConsole == null || variablesFromConsole.isEmpty()) {
                return variables;
            }
            String[] splitedByCommasInputString = variablesFromConsole.split(",");
            variables = new HashMap<>();
            for (String splitByDoublePoint : splitedByCommasInputString) {
                variables.put(splitByDoublePoint.split(":")[0].charAt(0), Double.parseDouble(splitByDoublePoint.split(":")[1]));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return variables;
    }

    private void run() {
        userDefVars = getVarsFromCmdLine();
        if (!(userDefVars == null || userDefVars.isEmpty())) {
            System.out.print("variables are ");
            System.out.println(userDefVars);
        }
        String formula = getFormula();
        while (!formula.equals("")) {
            System.out.println(calculateExpression(formula));
            formula = getFormula();
        }
    }

    public static void main(String[] args) {
        new Calculator().run();
    }

    interface IFunc {

        public String calculateFunct(String d);
    }
}
